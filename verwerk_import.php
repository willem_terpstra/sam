<?php

require_once 'autoload.php';

if (!$config = simplexml_load_file('MVC/sys/config/config.xml')) {
    trigger_error('Error reading XML file', E_USER_ERROR);
}
date_default_timezone_set('Europe/Paris');
mysql_connect("localhost", $config->mysql->user, $config->mysql->pwd);
mysql_select_db($config->mysql->db);
$importFile = new ImportFile($_GET['id']);
$lapInfo = $importFile->read();
$resultaat = new Resultaat();
$resultaat->calculate($lapInfo, $_GET['id']);
$importFile->setStatus('verwerkt');
$importFile->saveOrUpdate();
$importFileView = new ImportFileView($importFile->getUid());
$html = $importFileView->getFileRows();
echo $html;
?> 
