<?php

class RacedagKlasseController extends Controller {

    const SESSIES = "SESSIES";
    const ADD_ALL_KLASSES = "ADD_ALL_KLASSES";


    public function run($view, $uid, $action) {
        $post = Post::$postVars;
        $view = new RacedagKlasseView($uid);
        if ($action == self::SESSIES) {
            return $view->showSessies();
        }
        if ($action == self::ADD_ALL_KLASSES) {
            return $view->addAllKlasses();
        }

        $content = parent::run($view, $uid, $action);
        if ($content == false) {
            if (isset($post['act'])) {
                switch ($post['act']) {
// EXAMPLE:					
//					case self::ADD_PRODUCT:	
//						$content = $command->AddProduct();
//						break;

                    default:
                        throw new Exception("action {$post['act']} not defined");
                }
            }
            return $content;
        }

        return $content;
    }

}
