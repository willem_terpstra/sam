<?php

class RacedagController extends Controller {

    // Possible commands
    //	const ADD_PRODUCT = "ADD_PRODUCT";



    public function run($view, $uid, $action) {
        $post = Post::$postVars;
        if (is_string($view)) {
            $view = new $view($uid);
        }

        $this->view = $view;
        if(strtoupper($action) == Controller::DELETE) {
           return $view->delete();
        }
//        if ($action == self::BROWSELIJST) {
//            $content = $view->screen();
//        } else {
            $content = parent::run($view, $uid, $action);
//        }

        if ($content == false) {
            if (isset($post['act'])) {
                switch ($post['act']) {
// EXAMPLE:					
//					case self::ADD_PRODUCT:	
//						$content = $command->AddProduct();
//						break;

                    default:
                        throw new Exception("action {$post['act']} not defined");
                }
            }
            return $content;
        }

        return $content;
    }

}

?>