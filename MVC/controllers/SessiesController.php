<?php

class SessiesController extends Controller {
    // Possible commands

    const ADD_ALL_SESSIES = "ADD_ALL_SESSIES";

    public function run($view, $uid, $action) {
        $post = Post::$postVars;
        if(! $view instanceof SessiesView)
            $view = new SessiesView($uid);
        if ($action == self::ADD_ALL_SESSIES) {
            return $view->addAllSessies();
        }
        $content = parent::run($view, $uid, $action);

        if ($content == false) {
            if (isset($post['act'])) {
                switch ($post['act']) {
                    default:
                        throw new Exception("action {$post['act']} not defined");
                }
            }
            return $content;
        }

        return $content;
    }

}

