<?php 
class _MetaTableSettings extends dbTable	{
	/* db table */
	protected	$tableName = 'meta_table_settings';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'target_table' => false,
		'create_entity' => false,
		'create_controller' => false,
		'create_html' => false,
		'header_text' => false,
		'create_view' => false,
		'create_frontend' => false,	);

	/* properties */
	protected $uid;
	protected $target_table;
	protected $create_entity;
	protected $create_controller;
	protected $create_html;
	protected $header_text;
	protected $create_view;
	protected $create_frontend;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'target_table' => array('name' => 'target_table', 'type' => 'xsd:string'),
		'create_entity' => array('name' => 'create_entity', 'type' => 'xsd:string'),
		'create_controller' => array('name' => 'create_controller', 'type' => 'xsd:string'),
		'create_html' => array('name' => 'create_html', 'type' => 'xsd:string'),
		'header_text' => array('name' => 'header_text', 'type' => 'xsd:string'),
		'create_view' => array('name' => 'create_view', 'type' => 'xsd:string'),
		'create_frontend' => array('name' => 'create_frontend', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_MetaTableSettings";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"target_table" => $this->target_table, 
		"create_entity" => $this->create_entity, 
		"create_controller" => $this->create_controller, 
		"create_html" => $this->create_html, 
		"header_text" => $this->header_text, 
		"create_view" => $this->create_view, 
		"create_frontend" => $this->create_frontend, 
		);	  
	}

	
	/* FK methods */
	
}
?>