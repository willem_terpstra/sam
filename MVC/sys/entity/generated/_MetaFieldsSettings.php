<?php 
class _MetaFieldsSettings extends dbTable	{
	/* db table */
	protected	$tableName = 'meta_fields_settings';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'table_name' => false,
		'field_name' => false,
		'setting' => false,
		'value' => false,
		'validation' => false,	);

	/* properties */
	protected $uid;
	protected $table_name;
	protected $field_name;
	protected $setting;
	protected $value;
	protected $validation;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'table_name' => array('name' => 'table_name', 'type' => 'xsd:string'),
		'field_name' => array('name' => 'field_name', 'type' => 'xsd:string'),
		'setting' => array('name' => 'setting', 'type' => 'xsd:string'),
		'value' => array('name' => 'value', 'type' => 'xsd:string'),
		'validation' => array('name' => 'validation', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_MetaFieldsSettings";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"table_name" => $this->table_name, 
		"field_name" => $this->field_name, 
		"setting" => $this->setting, 
		"value" => $this->value, 
		"validation" => $this->validation, 
		);	  
	}

	
	/* FK methods */
	
}
?>