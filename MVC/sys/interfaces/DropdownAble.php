<?php
interface DropdownAble {
	public function getDropdownDefaultValue();
	public function getDropdownNullOption();
	public function getValue();
	
	// 
	public function getIntValueMethod();
}
?>