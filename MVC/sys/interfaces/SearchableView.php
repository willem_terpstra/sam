<?php
interface SearchableView {
	public function setAuth($auth); // browse or edit flag (see Controller)
	public function search($from);
	public function fill($data);	// to fill searchscreen with posted values  
}
?>