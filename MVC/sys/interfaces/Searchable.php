<?php
interface Searchable {
	public function getPostedStartdateField();
	public function getPostedEnddateField();
	public function getDatefieldToSearchOn();
}
?>