<?php 

echo "<?php\n";
?>

class _<?php echo $className ?>View extends View {	
	/**
	 *
	 * @var type <?php echo $className ?>
	 
	 */
	
	protected $<?php echo $className ?>;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this-><?php echo $className ?> = new <?php echo $className ?>($uid);
	}
	
	public function getInstance() {
		return $this-><?php echo $className ?>;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function get<?php echo $className ?> () {
		return $this-><?php echo $className ?>;
	}
	
	public function lijst($sql = null)	{	
		$<?php echo $className ?> = new <?php echo $className ?>();
		$<?php echo $className ?> = $<?php echo $className ?>->lijst($sql);
		$t = new TemplateEngine("<?php echo $className ?>Lijst.html");
		$t->assign("<?php echo $className ?>",$<?php echo $className ?>);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$<?php echo $className ?> = new <?php echo $className ?>();
		$<?php echo $className ?> = $<?php echo $className ?>->lijst($sql);
		$t = new TemplateEngine("browse<?php echo $className ?>Lijst.html");
		$t->assign("<?php echo $className ?>",$<?php echo $className ?>);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this-><?php echo $className ?>->getMessages());
		$t = new TemplateEngine("<?php echo $className ?>Detail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this-><?php echo $className ?>);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browse<?php echo $className ?>Detail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this-><?php echo $className ?>);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this-><?php echo $className ?>->validate($post);
		$m = $this-><?php echo $className ?>->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
<?php echo $setters ?>
		
		try {
			$this-><?php echo $className ?>->saveOrUpdate();
		} catch (dbTableException $e) {
			$this-><?php echo $className ?>->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this-><?php echo $className ?>->remove();
		return $this->quit();
	}
}

?>