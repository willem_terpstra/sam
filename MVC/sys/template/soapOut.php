<?php
if ( substr($varName,-3) == '_id') {
	$funcName = dbTable::camelCase($varName);
	$funcName = substr ($funcName,0,-2); 	// strip 'Id'
	$varName = substr ($varName,0,-3); 		// strip '_id'
?>		"<?php echo $varName; ?>" => $this->get<?php echo $funcName; ?>($this-><?php echo $varName; ?>_id),
<?php
} else {
?>		"<?php echo $varName; ?>" => $this-><?php echo $varName; ?>, 
<?php
}
?>