<?php
	$arr = explode ("_", $varName);
	if (strtolower($arr[count($arr) - 1]) == "id"  && count($arr) > 1 )
	{	$arr[count($arr) - 1] = "";
		$className = "";
		foreach ($arr as $a )
		{	$className .= ucFirst($a);
		}
?>		
		public function get<?php echo $className; ?>List ($sql = null)	{
			$<?php echo $className; ?> = new <?php echo $className; ?>();
			$list = array();
			$list = $<?php echo $className; ?>->lijst($sql);
			return $list;
		}
		
		public function get<?php echo $className; ?> ($uid = NULL)	{
			if(empty($uid)) {
				return new <?php echo $className; ?>($this->get<?php echo $className; ?>Id());
			}
			$<?php echo $className; ?> = new <?php echo $className; ?>($uid);
			return $<?php echo $className; ?>->getValue();
		}
<?php
	}
?>