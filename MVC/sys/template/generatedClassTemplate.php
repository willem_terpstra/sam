<?php echo "<?php " ?>

class <?php echo $className; ?> extends dbTable	{
	/* db table */
	protected	$tableName = '<?php echo $table_name?>';
	
	/* modification map */
	protected  $dbModified = array (
<?php echo $modifiedMap; ?>
	);

	/* properties */
<?php echo $privates; ?>

	/* soap declarations */
	public static $soapDeclarations = array(
<?php echo $soapDeclarations; ?>	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
<?php
		if(substr(strtolower($table_name),0,3) == 'vw_') {
?>
		$this->is_view = true;
<?php
		}
?>
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "<?php echo $className ?>";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
<?php echo $soapOut; ?>
		);	  
	}

	
	/* FK methods */
	<?php echo $fkMethods; ?>

}
<?php echo "?>"; ?>