<?php

class VwUitslagException extends Exception {
    const NOT_ENOUGH_LAPS = 1000;
    const NO_LAPS = 1001;
}