<?php

class Controller {
	// Possible commands
	const ADD			= "ADD";
	const BROWSE		= "BROWSE";
	const BROWSELIJST 	= "BROWSELIJST";
	const CANCEL		= "CANCEL";
	const DETAIL		= "DETAIL";
	const DELETE		= "DELETE";
	const EDITLIJST 	= "EDITLIJST";
	const LOAD			= "LOAD";
	const LOGIN			= "LOGIN";
	const LOGOUT		= "LOGOUT";
	const REMOVE		= "REMOVE";
	const SEARCH		= "SEARCH";
	const THANKS		= "THANKS";
	const UPDATE		= "UPDATE";

	
	protected $view = NULL;
	
	private $atomicCommands = array(self::REMOVE);
	
	public function __construct() {
	}

	public function run($view, $uid, $action )
	{	
		$post = Post::$postVars;
		if ( is_null ($view) )
		{	throw new Exception ("Cannot run controller without view");
		}
	
		if(is_null($uid)) {
			// POST
			if(isset($post['uid'])) {
				if(is_numeric($post['uid'])) {
					$uid = (int)$post['uid'];
				}
			}
		}
		
		if(array_key_exists('act', $post)) {
			switch ($post['act']) {
				case self::UPDATE :
				case self::REMOVE:
					$action = $post['act'];
					break;
			}
		}
		
		
		if(in_array($action, $this->atomicCommands)) {
			if(empty($uid)) {
				throw new Exception('uid mag niet leeg zijn voor de volgende commando\'s: '. var_Export($this->atomicCommands, true));
			} 
		}
		if(is_string($view)) {
			$view = new $view($uid);
		}
		$this->view = $view;
			
		switch (strtoupper($action))
		{	case self::EDITLIJST:	
				$content = $view->lijst();
				break;
			case self::BROWSELIJST:
				$content = $view->browseLijst();
				break;
			case self::DETAIL:
				$content = $view->detail($uid);
				break;
			case self::BROWSE:
				$content = $view->browseDetail($uid);
				break;
			case self::UPDATE:
				if($view->validate($post)) {
					$content = $view->save($uid);
				} else {
					$content = $view->detail($uid);
				}
				break;
			case self::REMOVE	:
				$content = $view->remove($uid);
				break;
			case self::CANCEL:
				
				$content = $view->quit();
				break;
			default:
				throw new Exception ("Cannot run controller with action: [$action]");
		}
		if(!is_string($content) && $content !== false) {
			throw new Exception ("Controller returned unacceptable value: [". $content ."] performing action: [$action]");
		}

		return $content;

	} 

}

?>