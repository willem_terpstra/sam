<?php

class Logger {
   
	const DEBUG = 0;
	const WARNING = 1;
	const ERROR = 2;
	const FATAL = 3;
	
    public static function str2hex($string) {
    	$hex='';
    	$sep = '';
	    for ($i=0; $i < strlen($string); $i++)
	    {
	        $hex .= $sep. dechex(ord($string[$i])) . '['.$string[$i].']';
	        $sep = '.';
	    }
    	return $hex;
    }

    public static function hex2str($string) {
    	$hex = explode('.',$string);
        $a = array();
    	foreach($hex as $h ){
    		if(empty($h)) continue;
    		$t = hexdec($h);
    		$c = chr($t);
    		$a[] = $c;
    	}
    	$asc = implode('',$a);
    	return $asc;
    }
    
    static public function write($msg = '', $severity = Logger::DEBUG) {
		$bcktrace = debug_backtrace();
		$function = 'main';
		if(array_key_exists(1,$bcktrace)) {
			$function = $bcktrace[1]['function'];	
		}
		
		$line = array();
		$callbacks = 15;
		if(count($bcktrace) < 15) {
			$callbacks = count($bcktrace);
		}
		for($i=0;$i<$callbacks;$i++) {
			$line[] = $bcktrace[$i]['file'] . ' ' . $bcktrace[$i]['line'];
		}
		$calls = implode('<br/>', $line);

    	$log = new Log();
    	$log->setIp($_SERVER['REMOTE_ADDR']);
	   	$log->setUserAgent($_SERVER['HTTP_USER_AGENT']);
//    	$msg = "$calls $function:<br/>$msg";
//    	$msg = str_replace('\\','\\\\',$msg);    	
    	if($severity > Logger::WARNING) {
    		$msg = "<SPAN style=\"color: red\" >$msg</SPAN>";
    	}
    	$log->setLogmessage($msg);
    	$log->saveOrUpdate();
    }
}

?>