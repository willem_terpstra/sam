<?php
class Post implements ArrayAccess {
	
	public static $postVars = array();
	
	function __construct() {
//		self::$postVars = $_POST;

		
		if(isset($_POST)) {
			self::$postVars = $this->cleanData($_POST);
			unset ($_POST);
		}
	}
	
	private function cleanData($data) {
		if(is_array($data)) {
			foreach($data as $key => $value) {
				$data[$key] = $this->cleanData($value);
			}
			return $data;
		}
		$data = trim($data);
		$data = htmlentities($data);
		$data = addslashes(mysql_real_escape_string($data));
		return $data;
	}
		
	
	
	public function offsetExists( $offset )	{
		return isset( self::$postVars[$offset] );
	}
	
	public function offsetSet( $offset, $value)	{
		self::$postVars[$offset] = $value;
	}
	public function offsetGet( $offset )	{	
		if(!array_key_exists($offset, self::$postVars)) {
			return NULL;
		}
		return self::$postVars[$offset];
	}
	public function offsetUnset( $offset )	{
		unset( self::$postVars[$offset] );
	}
	
}


$post = new Post();

?>