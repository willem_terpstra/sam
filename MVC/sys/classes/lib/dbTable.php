<?php

abstract class dbTable implements Iterator, Countable {

    const EXACT_MATCH = 1;
    const NON_EXACT_MATCH = 2;
    const DATE_MATCH = 3;

    private $errNo;
    private $errMsg;
    protected $pk = null;
    protected $fk = null;
    protected $hasFks = null;
    protected $messages = array();
    protected $meta = array();
    protected $fieldsInfo = array();
    private $last_query = null;
    protected $is_view = false;
    static $instance_id;
    // iterator	
    private $record;
    private $collection;
    private $pointer = 0;
    private $numRows = 0;

//	abstract protected function getModifiedMap();
    abstract protected function getTableName();

    // flags

    const CAMELCASE = 1;
    // errors
    const NO_DEFAULT_VALUE = 1364;

    public function __construct($str = null) {
        self::$instance_id++;

        if (!is_null($str) && trim($str) != '') {
            if (ctype_digit($str)) {
                $this->load($str);
            } else {
                
            }
        }
        // load meta data
        $sql = "SELECT * FROM meta_fields_settings WHERE table_name ='{$this->getTableName()}'";
        $rs = $this->execSql($sql);
        if (mysql_num_rows($rs) > 0) {
            while ($row = mysql_fetch_assoc($rs)) {
                $this->meta[$row['table_name']][$row['field_name']][$row['setting']] = $row['value'];
                $this->meta[$row['table_name']][$row['field_name']]['validation'] = $row['validation'];
            }
        }
    }

    private function setRecord($data) {
        $this->record = $data;
    }

    public function setMessage($msg) {
        if (is_array($msg)) {
            $this->messages = array_merge($this->messages, $msg);
        } else {
            $this->messages[] = $msg;
        }
    }

    public function getMessages() {
        return $this->messages;
    }

    public function getErrNo() {
        $errNo = $this->errNo;
        $this->errNo = 0;
        return $errNo;
    }

    private function loadFieldInfo() {
        $rs = $this->execSql("SHOW FIELDS FROM {$this->getTableName()}");
        while ($row = mysql_fetch_assoc($rs)) {
            $this->fieldsInfo[$row['Field']]['Type'] = $row['Type'];
            $this->fieldsInfo[$row['Field']]['Null'] = $row['Null'];
        }
    }

    private function isDate($field) {
        if (empty($this->fieldsInfo)) {
            $this->loadFieldInfo();
        }
        return $this->fieldsInfo[$field]['Type'] == 'date';
    }

    private function isText($field) {
        if (empty($this->fieldsInfo)) {
            $this->loadFieldInfo();
        }
        return $this->fieldsInfo[$field]['Type'] == 'text';
    }

    private function dateNlToMysql($date, $field) {
        if (empty($date)) {
            throw new dbTableException('Empty date:' . $field);
        }
        $date = str_replace(array('/', ' '), '-', $date);
        $date = explode('-', $date);
        $date = $date[2] .'-' . $date[1] . '-' . $date[0];
        return $date;
    }

    public function clear() {
        foreach ($this->getModifiedPropertys() as $prop) {
            $this->$prop = NULL;
        }
    }

    public function fill($row) {
        if (empty($this->fieldsInfo)) {
            $this->loadFieldInfo();
        }

        foreach ($row as $field => $value) {
            try {
                $this->$field = $value;
                if (property_exists($this, 'dbModified')) {
                    $this->dbModified[$field] = true;
                }
            } catch (dbTableException $e) {
                if ($e->getCode() == dbTableException::PROPERTY_DOES_NOT_EXIST) {
                    // try set function
                    $func = 'set' . $this->camelCase($field);
                    $this->$func(stripslashes($value));
                } else {
                    throw new Exception($e->getMessage(), $e->getCode());
                }
            }
        }
    }

    public function getModifiedMap() {
        return $this->dbModified;
    }

    public function getFields() {
        return array_keys($this->dbModified);
    }

    public static function toArray($dbTableObj) {
        $a = array();
        $fields = $dbTableObj->getFields();
        foreach ($dbTableObj as $record) {
            $row = array();
            foreach ($fields as $value) {
                $row[$value] = $record->$value;
            }
            $a[] = $row;
        }
        return $a;
    }

    public static function CC2U($word) {
        $word = ucfirst($word);
        $result = array();
        preg_match_all('/(?P<strs>(?P<str>[A-Z][a-z]*))(?P<num>[\d]{0,2})/', $word, $result, PREG_PATTERN_ORDER);
        $result = array_filter(array_merge($result['strs'], $result['num']));
        $property = strtolower(implode('_', $result));
        return $property;
    }

    public function __set($name, $value) {
        throw new dbTableException('cannot set property ' . $name . ', does not exist in: ' . $this->getTableName(), dbTableException::PROPERTY_DOES_NOT_EXIST);
    }

    public function __get($name) {
        throw new Exception('cannot get property ' . $name . ', does not exist in: ' . $this->getTableName());
    }

    public function __call($name, $args) {

        if (preg_match('/(?P<function>set|get|loadBy)(?P<property>\w+)/', $name, $regs)) {
            $property = $regs['property'];
            $function = $regs['function'];
        }
        $property = self::CC2U($property);
        if (property_exists($this, $property)) {

            switch ($function) {
                case 'get' :
                    return $this->$property;
                case 'set':
                    if ($this->$property !== $args[0]) {
                        if (!array_key_exists($property, $this->dbModified)) {
                            Logger::write('Property ' . $name . ' not found in modifiedMap for table ' . $this->getTableName(), Logger::ERROR);
                            throw new Exception('Property ' . $name . ' not found in modifiedMap for table ' . $this->getTableName());
                        }
                        $this->dbModified[$property] = true;
                        $this->$property = $args[0];
                    }
                    return;
                case 'loadBy':
                    $property = self::CC2U(substr($name, 6));
                    $instanceName = $this->getClassName();
                    $ids = $args[0];
                    if (is_array($args[0])) {
                        $ids = implode('\', \'', $args[0]);
                    }
                    $sql = 'SELECT * FROM ' . $this->getTableName() . ' WHERE ' . $property . ' IN (\'' . $ids . '\')';
                    $l = $this->lijst($sql);
                    if (count($l) == 1) {
                        return $this->first();
                    } else {
                        return $l;
                    }
            }
        }

        throw new dbTableException('Non existing method ' . $name . ' called for non existing property ' . $property . ' in table ' . $this->getTableName(), dbTableException::METHOD_DOES_NOT_EXIST);
    }

    function current() {
        $row = mysql_fetch_array($this->collection, MYSQL_ASSOC);
        $class = $this->getClassName();
        $this->setRecord(new $class($row[$this->getPK()]));
        return $this->record;
    }

    function next() {
        ++$this->pointer;
        if ($this->valid()) {
            mysql_data_seek($this->collection, $this->pointer);
        }
    }

    function first() {
        $this->rewind();
        return $this->current();
    }

    function key() {//	echo "Key Not implemented";
    }

    function valid() {
//		echo "rows: " . $this->numRows . " pointer: {$this->pointer}<br/>";
        return ( $this->pointer < $this->numRows );
        //	echo "Valid Not implemented";
    }

    function rewind() {
        if ($this->numRows > 0) {
            mysql_data_seek($this->collection, 0);
        }
        $this->pointer = 0;
    }

    function offsetExists($index) {
        /* 		
          if(!is_numeric($index)) {
          return array_key_exists($index, $this->dbModifiedMap);
          }

          if($index < 0) {
          return false;
          }
          if($index > ($this->numRows-1)) {
          return false;
          }
          return true;
         */
        throw new Exception('offsetExsists not implemented');
    }

    function offsetGet($index) {
        if (!is_numeric($index)) {
            $func = 'get' . $this->camelCase($index);
            return $this->record->$func();
        }

        if ($this->numRows > 0) {

            $this->pointer = $index;
            if ($this->valid()) {
                mysql_data_seek($this->collection, $this->pointer);
                $row = mysql_fetch_array($this->collection, MYSQL_ASSOC);
                $class = $this->getClassName();
                $this->setRecord(new $class($row[$this->getPK()]));
            }
        }
        return $this;
    }

    function offsetSet($index, $newValue) {
        throw new Exception('This implementation of ArrayAccess cannot set values');
    }

    function offsetUnset($index) {
        throw new Exception('This implementation of ArrayAccess cannot unset values');
    }

    public function saveOrUpdate() {
        $uid = $this->getUid();
        if (empty($uid)) {
            $this->save();
        } else {
            $this->update();
        }
    }

    public function fillEmptyString() {
        $fieldList = $this->getModifiedMap();
        foreach ($fieldList as $field => $value) {
            $this->$field = '';
        }
    }

    protected function save($autoIncrement = true) {
        $sql = "INSERT INTO {$this->getTableName()} ";


        $fieldNames = array();
        $values = array();
        $newUid = false;
        foreach ($this->getModifiedMap() as $field => $modified) {
            if ($modified) {
                $value = $this->$field;
                if ($this->isDate($field)) {
                    $value = $this->dateNlToMysql($value, $field);
                }
                $escapedValue = mysql_real_escape_string($value);
                if (array_key_exists($this->getTableName(), $this->meta)) {
                    if (array_key_exists($field, $this->meta[$this->getTableName()])) {
                        if (array_key_exists('saveAsIs', $this->meta[$this->getTableName()][$field])) {
                            if ($this->meta[$this->getTableName()][$field]['saveAsIs']) {
                                $value = preg_replace('/(\\\\+[r|n])/', '', $value);
                                $escapedValue = $value;
                            }
                        }
                    }
                }
                $value = $escapedValue;
                $value = "'$value'";
                $fieldNames[] = $field;
                $values[] = $value;
            }
        }
        $fieldNames = implode(',', $fieldNames);
        $values = implode(',', $values);
        $sql .= "($fieldNames) VALUES ($values)";
        try {
            $this->execSql($sql);

            if ($newUid) {
                $this->setUid($newUid);
            } else {
                $this->setUid(mysql_insert_id());
            }
        } catch (dbTableException $e) {
            switch ($e->getCode()) {
                case dbTableException::DUPLICATE_ENTRY:
                    throw new dbTableException('Dit record bestaat al.Het systeem heeft voorkomen dat deze gegevens dubbel werden ingebracht', dbTableException::DUPLICATE_ENTRY);
                    break;
                default:
                    throw new dbTableException($e->getMessage(), $e->getCode());
            }
        } catch (Exception $e) {
            if ($this->errNo == self::NO_DEFAULT_VALUE) {
                $this->save(false); // auto_increment not defined on PK
            } else {
                throw($e);
            }
        }
        return;
    }

    public function update() {
        $sql = "UPDATE {$this->getTableName()} SET ";

        $values = "";
        $updates = array();

        foreach ($this->getModifiedMap() as $field => $modified) {
            if ($modified) {
                $value = $this->$field;
                if ($this->isDate($field)) {
                    $value = $this->dateNlToMysql($value, $field);
                }
                if ($this->isText($field)) {
//					$value = preg_replace('/(\\\\+[r|n])/', '', $value);
//					$value = str_replace(array("\r\n", "\r", "\n", "\t"), '', $value);
                    $value = str_replace(array('<p>'), '', $value);
                }
                $escapedValue = mysql_real_escape_string($value);
                if (array_key_exists($this->getTableName(), $this->meta)) {
                    if (array_key_exists($field, $this->meta[$this->getTableName()])) {
                        if (array_key_exists('saveAsIs', $this->meta[$this->getTableName()][$field])) {
                            if ($this->meta[$this->getTableName()][$field]['saveAsIs']) {
                                $value = preg_replace('/(\\\\+[r|n])/', '', $value);
                                $value = preg_replace('/\\\\+/', '\\', $value);
                                $escapedValue = $value;
                            }
                        }
                    }
                }
                $value = $escapedValue;
                $updates[] = "$field = '$value'";
            }
        }
        if (count($updates) == 0)
            return;
        $updates = implode(', ', $updates);

        $sql .= $updates;
        $sql .=" WHERE	{$this->getPK()}='{$this->getUid()}'";
        $this->execSql($sql);
    }

    public function remove() {
        $this->execSql("DELETE FROM {$this->getTableName()} WHERE {$this->getPK()}='{$this->getUid()}'");
    }

    public function execSql($sql) {
        $rs = mysql_query($sql);
        $this->last_query = $sql;
        $this->errNo = mysql_errno();
        $this->errMsg = mysql_error();
        if ($this->errNo != 0) {
            throw new dbTableException("{$this->errNo}: {$this->errMsg} <br/><pre>$sql</pre>", $this->errNo);
        }

        return $rs;
    }

    /* load method */

    public function load($id) {
        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE " . $this->getPK() . "='" . mysql_real_escape_string($id) . "' LIMIT 1";
        $rs = mysql_query($sql);
        if (mysql_num_rows($rs) == 0) {
            throw new dbTableException("load $id failed. $sql", dbTableException::LOAD_FAILED_EXCEPTION);
        }
        $this->numRows = 1;
        $row = mysql_fetch_assoc($rs);
        $this->fill($row);
    }

    /**
     * geeft instance terug waarvan de property $propertyName de waarde propertyValue heeft
     *
     * @param string $propertyName
     * @param string $propertyValue
     * @return false indien niet gevonden, anders true
     */
    public function loadByProperty($propertyName, $propertyValue) {
        $sql = "SELECT uid FROM {$this->getTableName()} WHERE $propertyName='$propertyValue'";

        $rs = $this->execSql($sql);
        if (mysql_num_rows($rs) == 1) {
            $row = mysql_fetch_assoc($rs);
            $uid = $row['uid'];
            $this->load($uid);
            return true;
        }
        return false;
    }

    protected function getPK() {
        if ($this->is_view) {
            return 'uid';
        }
        if (!is_null($this->pk))
            return $this->pk;

        $sql = "SHOW INDEX FROM  " . $this->getTableName();
        $rs = $this->execSql($sql);
        while ($row = mysql_fetch_array($rs)) {
            if ($row['Key_name'] == 'PRIMARY') {
                $pk = $row['Column_name'];
            }
        }

        if (is_null($pk)) {
            Logger::write("Table not found or primary key not defined (view must set is_view to true, viewname must start with vw_): " . $this->getTableName());
            throw new Exception("<strong>Primary key not defined for table (view must set is_view, viewname must start with vw_): " . $this->getTableName() . "</strong>");
        }


        $this->pk = $pk;
        return $pk;
    }

    /**
     * 
     * 
     */
    public function lijst($data = null) {
        $this->collection = NULL;
        if (is_null($data)) {
            $data = "SELECT * FROM " . $this->getTableName() . " ";
        } elseif (is_array($data)) {
            $data = "SELECT * FROM " . $this->getTableName() . " WHERE " . $this->getPK() . " IN(" . implode(',', $data) . ')';
        }
        $rs = $this->execSql($data);
        $this->numRows = mysql_num_rows($rs);
        $this->collection = $rs;

        return $this;
    }

    public function getLabel($field) {
        if (isset($this->meta[$this->getTableName()][$field]['label_nl'])) {
            return $this->meta[$this->getTableName()][$field]['label_nl'];
        } else {
            $field = ucfirst(str_replace('_', ' ', $field));
            return $field;
            // TODO : debug setting
//			return '<!-- '. $this->getTableName() .' -->'. $field;
        }
    }

    private function isHidden($field) {
        if (isset($this->meta[$this->getTableName()][$field]['hidden'])) {
            return $this->meta[$this->getTableName()][$field]['hidden'];
        } else {
            return false;
        }
    }

    private function isObliged($field) {
        if (!empty($this->meta)) {
            if (array_key_exists($field, $this->meta[$this->getTableName()])) {
                if (array_key_exists('obliged', $this->meta[$this->getTableName()][$field])) {
                    return $this->meta[$this->getTableName()][$field]['obliged'];
                }
            }
        }
        return false;
    }

    public function getChildren($orderBy = null) {
        $sqlOrderBy = '';
        if (!is_null($orderBy)) {
            $sqlOrderBy = " ORDER BY $orderBy";
        }
        $sql = 'SELECT * FROM ' . $this->getTableName() . ' WHERE pid = ' . $this->getUid() . $sqlOrderBy;
        $this->lijst($sql);
        foreach ($this as $instance) {
            $instance->getChildren();
        }
        $this->first();
        return $this;
    }

    public function getHtmlType($field) {
        if ($this->isHidden($field)) {
            return 'hidden';
        } elseif (isset($this->meta[$this->getTableName()][$field]['type'])) {
            return $this->meta[$this->getTableName()][$field]['type'];
        } else {
            return 'text';
        }
    }

    public function getHtmlSpecialAttributes($field) {
        $type = $this->getHtmlType($field);
        switch ($type) {
            case 'checkbox';
                if ($this->$field == '1') {
                    return ' checked="checked" ';
                }
            default:
                return '';
        }
    }

    private function getMaxPk() {
        $sql = "SELECT MAX({$this->getPK()}) FROM {$this->getTableName()}";

        $rs = mysql_query($sql);
        $row = mysql_fetch_array($rs);
        return $row[0];
    }

    public function getForeignKey($fieldName) {
        $this->getForeignKeys();
        if ($this->hasFks) {
            foreach ($this->fk as $fk) {
                if ($fieldName == $fk["FK"]) {
                    return $fk;
                }
            }
        }
        return null;
    }

    public function isSelfReferring() {
        $this->getForeignKeys();
        if ($this->hasFks) {
            foreach ($this->fk as $fk) {
                if ($fk['SELF_REFERRING']) {
                    return true;
                }
            }
        }
        return false;
    }

    private function getForeignKeys() {
        if (!is_null($this->hasFks)) {
            return $this->fk;
        }

        $sql = "SHOW CREATE TABLE `{$this->getTableName()}`";

        $rs = mysql_query($sql);
        $row = mysql_fetch_array($rs);
//var_export($row);		
        $data = $row['Create Table'];
        // split up:
        // InnoDB free: 11264 kB; (`ckr_info_id`) REFER `swol/ckr_info`(`id`)
//		$data = str_replace ("`","",$data); 
        $arr = explode("CONSTRAINT", $data);
        array_shift($arr); // first element is not a constraint, remove
        $fkCount = 0;
        $fk = null;
        $this->hasFks = false;
        foreach ($arr as $el) {
            $c = explode("`", $el);
            $fk[$fkCount]['FK'] = $c[3];
            $fk[$fkCount]['REF'] = "{$c[5]}.{$c[7]}";
            $fk[$fkCount]['CLASS'] = $this->camelCase($c[5]);
            $fk[$fkCount]['SELF_REFERRING'] = false;
            if ($c[5] == $this->getTableName()) {
                $fk[$fkCount]['SELF_REFERRING'] = true;
                Logger::write(strtoupper($this->getTableName()) . ' = SELF REFERING<br/>');
            }
            $fkCount++;
            $this->hasFks = true;
        }
        $this->fk = $fk;
        if (is_null($this->hasFks)) {
            $this->hasFks = false;
        }
    }

    static public function getTables($camelCase = false) {
        $tables = array();

        $q = mysql_query("SHOW TABLES");
        while ($r = mysql_fetch_array($q)) {

            if ($camelCase) { // to get class names
                $tables[] = self::camelCase($r[0]);
            } else {
                $tables[] = $r[0];
            }
        }
        return $tables;
    }

    static public function exists($tbl) {
        $tables = self::getTables();
        if (in_array($tbl, $tables)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    static public function camelCase($name) {
        $arr = explode("_", $name);
        $result = "";
        foreach ($arr as $part) {
            $result .= ucfirst($part);
        }
        $result = ucfirst($result);

        return $result;
    }

    static public function getField($sql) {
        $rs = mysql_query($sql);

        $errNo = mysql_errno();
        $errMsg = mysql_error();
        if ($errNo != 0) {
            throw new Exception("{$errNo}: {$errMsg} <br/><pre>$sql</pre>");
        }

        return (mysql_result($rs, 0, 0));
    }

    public function count() {
        if (empty($this->numRows)) {
            return 0;
        }
        return $this->numRows;
    }

    public function getNumRows() {
        return $this->count();
    }

    public function getModifiedPropertys() {
        $modified = array();
        foreach ($this->getModifiedMap() as $prop => $value) {
            if ($value == true) {
                $modified[$prop] = true;
            }
        }
        return $modified;
    }

    public static function copyFields($source, &$target, $sourcePk = 'uid') {
        $mapA = $source->getModifiedMap(); // alle variablelen CursusDatum
        $mapB = $target->getModifiedMap();
        $map = array_intersect_key($mapB, $mapA);

        unset($map[$sourcePk]); // niet de pk
        foreach ($map as $field => $dummy) {    // namen cursus datum velden in var $field
            $get = 'get' . dbTable::camelCase($field);  // naam getter cursusDatums
            $set = 'set' . dbTable::camelCase($field);  // naam setter aanmelden entity
            // kopieren:
            $target->$set($source->$get());
        }
    }

    public static function replaceEmptyFields($source, &$target, $sourcePk = 'uid') {
        $mapA = $source->getModifiedMap(); // alle variablelen CursusDatum
        $mapB = $target->getModifiedMap();
        $map = array_intersect_key($mapB, $mapA);

        unset($map[$sourcePk]); // niet de pk
        foreach ($map as $field => $dummy) {    // namen cursus datum velden in var $field
            $get = 'get' . dbTable::camelCase($field);  // naam getter cursusDatums
            $set = 'set' . dbTable::camelCase($field);  // naam setter aanmelden entity
            // kopieren:
            $targetValue = $target->$get();
            if (empty($targetValue)) {
                $target->$set($source->$get());
            }
        }
    }

    // TODO: in progress
    static public function columnSort($unsorted, $column) {
        $sorted = $unsorted;
        $size = $sorted->getNumRows() - 1;

        for ($i = 0; $i < $size; $i++) {
            for ($j = 0; $j < $size - $i; $j++) {
                if ($sorted[$j][$column] > $sorted[$j + 1][$column]) {
                    $tmp = $sorted[$j];
                    $sorted[$j] = $sorted[$j + 1];
                    $sorted[$j + 1] = $tmp;
                }
            }
        }
        return $sorted;
    }

    public function validate($post) {
        if (empty($this->fieldsInfo)) {
            $this->loadFieldInfo();
        }
        foreach ($this->fieldsInfo as $field => $descr) {

            $obliged = $this->isObliged($field);
            if ($obliged !== false && (!isset($post[$field]) || empty($post[$field]) )) {
                $this->setMessage($obliged);
            }

            $fieldName = $field;
            $type = $descr['Type'];
            if (preg_match('/(?P<type>\w+)?\(/s', $type, $match)) {
                $name = $match['type'];
            } elseif (preg_match('/(?P<type>\w+)/s', $type, $match)) {
                $name = $match['type'];
            }
            if (preg_match('/.*?\((?P<param>.*)?\)/s', $type, $match)) {
                $param = $match['param'];
            } else {
                $param = "";
            }
            $func = 'validate' . dbTable::camelCase($field);
            if (method_exists($this, $func)) {
                return $this->$func($post[$field]);
            } else {
                if (!empty($this->meta[$this->getTableName()][$field]['validation'])) {
                    if (array_key_exists('label_nl', $this->meta[$this->getTableName()][$field])) {
                        $fieldName = $this->meta[$this->getTableName()][$field]['label_nl'];
                    }
                    switch ($this->meta[$this->getTableName()][$field]['validation']) {
                        case 'telefoon':
                            $regexp = '/\d{10}/';
                            break;
                        case 'mobiel':
                            $regexp = '/06\d{8}/';
                            break;
                        case 'postcode':
                            $regexp = '/\d{4}[a-z|A-Z]{2}/';
                            break;
                        case 'datum_nl':
                            $regexp = '/\d{1,2}-\d{1,2}-\d{4}/';
                            break;
                        case 'email':
                            $regexp = '/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i';
                            break;
                    }
                } else {
                    switch ($name) {
                        case 'int':
                        case 'tinyint':
                            $regexp = '/\d*/';
                            break;
                        case 'float':
                            $regexp = '/[\d|\.]+/';
                            break;
                        case 'char':
                        case 'varchar':
                            $regexp = '/.{0,' . $param . '}/';
                            break;
                        case 'date':
                            $regexp = '%[0-3]*[0-9][/|-][0-1]*[0-9][/|-](20|19)[0-9][0-9]%';
                            break;
                        default:
                            $regexp = NULL;
                    }
                }
                if ($regexp === NULL) {
                    continue;
                }
                if (!empty($post[$field]) && array_key_exists($field, Post::$postVars) && !preg_match($regexp, $post[$field])) {

                    $this->messages[] = "\"{$post[$field]}\" is geen correcte $fieldName waarde";
                } else {
//					$this->messages[] = "$name $field -> [{$post[$field]}] MATCHES $regexp";
                }
            }
        }
        return(empty($this->messages));
    }

    private function createWheres($from) {

        $dates = array();
        $wheres = array();
        $searchDateField = NULL;
        if ($this->getPostedStartdateField() == $this->getDatefieldToSearchOn() ||
                $this->getPostedEnddateField() == $this->getDatefieldToSearchOn()) {
            throw new Exception('fout: $this->getPostedEnddateField() == $this->getDatefieldToSearchOn()');
        }
        if (array_key_exists($this->getDatefieldToSearchOn(), $from)) {
            $searchDateField = $from[$this->getDatefieldToSearchOn()];
//			unset($from[$this->getDatefieldToSearchOn()]);
        }

        foreach ($from as $field => $value) {
            $match = NULL;
            if (array_key_exists($field, $this->dbModified) && !empty($value)) {
                $dateSearch = ($field == $this->getPostedStartdateField() || $field == $this->getPostedEnddateField());
                // int value: exact match
                if (is_numeric($value)) {
                    if ($dateSearch) {
                        continue;
                    }
                    if ((int) $value == $value) {
                        $match = self::EXACT_MATCH;
                    }
                }
                if ($match == NULL) {
                    if ($field == $this->getPostedStartdateField() || $field == $this->getPostedEnddateField()) {
                        if (preg_match('/\A\d+-\d+-\d+\Z/', $value)) {
                            // date
                            $date = preg_replace('/(\d+)-(\d+)-(\d+)/', '$3-$2-$1', $value);
                            $dates[$field] = $date;
                            $match = self::DATE_MATCH;
                            continue;
                        }
                    }
                }
                if ($dateSearch) {
                    $this->setMessage('datum formaat is dd-mm-jjjj');
                    continue;
                }

                if ($match == NULL) {
                    // some string
                    $value = mysql_real_escape_string($value);
                    $match = self::NON_EXACT_MATCH;
                }
                switch ($match) {
                    case self::EXACT_MATCH:
                        $wheres[] = "$field = '$value'";
                        break;
                    case self::NON_EXACT_MATCH:
                        $wheres[] = "$field LIKE '%$value%'";
                        break;
                }
            }
        }

        if (!empty($dates)) {
            if (array_key_exists($this->getPostedStartdateField(), $dates) && array_key_exists($this->getPostedEnddateField(), $dates)) {
                if (!empty($searchDateField)) {
                    $w = array();
                    $w[] = "$searchDateField BETWEEN '{$dates[$this->getPostedStartdateField()]}' AND '{$dates[$this->getPostedEnddateField()]}'";
                    $wheres = array_merge($w, $wheres);
                }
            } else {
                if (array_key_exists($this->getPostedStartdateField(), $dates) || array_key_exists($this->getPostedEnddateField(), $dates)) {
                    $f1 = $dates[$this->getPostedStartdateField()];
                    $f2 = NULL;
                    if (array_key_exists($this->getPostedEnddateField(), $dates)) {
                        $f2 = $dates[$this->getPostedEnddateField()];
                    }

                    if (!empty($f1)) {
                        $date = $f1;
                        $operator = '>=';
                    } else {
                        $date = $f2;
                        $operator = '<=';
                    }

                    if (!empty($searchDateField) && !empty($date)) {
                        $w = array();
                        $w[] = "$searchDateField $operator '$date'";
                        $wheres = array_merge($w, $wheres);
                    }
                }
            }
        }

        return $wheres;
    }

    public function getSearchSql($from) {
        $wheres = $this->createWheres($from);
        $sql = 'SELECT * FROM ' . $this->getTableName() . ' WHERE 1=1 ';
        if (!empty($wheres)) {
            $wheres = ' AND ' . implode(' AND ', $wheres);
        } else {
            $wheres = '';
        }
        $sql = "$sql $wheres";

        return $sql;
    }

    public function debug($field) {
        $s = "\n** instance: " . self::$instance_id . " ****************\n";
        $d = $this;
        $d->first();
        foreach ($d as $e) {
            $s.= $e->$field . "\n";
        }
        $d->first();
        $s .= "\n******************\n";
        return $s;
    }

}

class dbTableI extends dbTable {

    protected $tableName = 'dbTableI';

    protected function getTableName() {
        return $this->tableName;
    }

}

function sqlGet($sql) {
    static $db = NULL;
    if (empty($db)) {
        $db = new dbTableI();
    }
    $r = NULL;
    $vars = get_defined_vars();
    $r = mysql_fetch_assoc($db->execSql($sql));
    if($r === false) 
        return NULL;
    extract($r);
    $newVars = get_defined_vars();
    unset($newVars['vars']);
    $diff = array();
    foreach ($newVars as $key => $value) {
        if (!array_key_exists($key, $vars)) {
            $diff[$key] = $value;
        }
    }
    if (count($diff) == 1) {
        return reset($diff);
    } else {
        return $diff;
    }
}
