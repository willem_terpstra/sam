<?php

class View {

    protected $messages = array();

    public function getTitle($headTitle, $action = NULL) {
        if (strpos($headTitle, ',') !== false) {
            $headTitles = explode(',', $headTitle);
        } else {
            $headTitles = array($headTitle);
        }
        foreach ($headTitles as $headTitle) {

            $str = array();
            $instr = explode(' ', $headTitle);
            foreach ($instr as $headTitle) {
                if (preg_match('/\[(\w+)\]/', $headTitle, $regs)) {
                    $field = $regs[1];
                    $func = 'get' . camelCase($field);
                    $r = NULL;
                    try {
                        $r = $this->getInstance()->$func();
                        if ($r === NULL) {
                            $str = NULL;
                            break;
                        }
                        $str[] = $r;
                    } catch (dbTableException $e) {
                        $str = '';
                    }
                } else {
                    $str[] = Strings::show($headTitle);
                }
            }
            if (!empty($str)) {
                return implode(' ', $str);
            }
        }
        return NULL;
    }

    public function getMetaDescription($tags, $action = NULL) {
        return $this->getTitle($tags, $action);
    }

    public function getContentTitle($title = NULL, $action = NULL) {
        return parent::getTitle($title, $action);
    }

    public function getHeadTitle($title = NULL, $action = NULL) {
        return parent::getTitle($title, $action);
    }

    public function getMessagesAsStrings() {
        $str = array();
        $messages = $this->getMessages();
        foreach ($messages as $message) {
            $str[] = $message->__toString();
        }
        return $str;
    }

    public function getErrorDivId() {
        if (Controller::isJson()) {
            if (isset(Get::$getVars['elementName'])) {
                if (method_exists($this, 'getChildErrorDiv')) {
                    return $this->getChildErrorDiv(Get::$getVars['elementName']);
                }
            }
        }
        return get_class($this->getInstance()) . 'Result';
    }

}

