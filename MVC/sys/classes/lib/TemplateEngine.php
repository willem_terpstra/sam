<?php

class TemplateEngine {

    const NULL_NOT_ALLOWED = 0;
    const NULL_ALLOWED = 1;
    const PAGINATOR_PAGES = 4;

    // indention for self referencing tables
    const TABLE_INDENTION = 1;
    const SPACE_INDENTION = 2;
    const OPTION_INDENTION = 3;

    private $template;
    private $variables = array();
    private $search = null;
    private $replace = null;
    static private $paginatorHtml = '';
    static private $paginatorPages = array();
    static private $numPages;
    static private $pages = array();

    public function __construct($template) {
        $this->template = $template;
    }

    public function setReplace($search, $replace) {
        $this->search = $search;
        $this->replace = $replace;
    }

    public function assign($varname, $var, $allowNull = self::NULL_NOT_ALLOWED) {
        if ($allowNull == self::NULL_NOT_ALLOWED) {
            if (is_null($var)) {
                throw new Exception("cannot assign null variable <b>$varname</b> to TemplateEngine");
            }
        }

        if (!is_null($this->search)) {
            $var = str_replace($this->search, $this->replace, $var);
        }

        $this->variables[$varname] = $var;
    }

    public function execTemplate($parseMetaPhp = true) {
        global $config;
        $contentT = '';
        // register variables local
        foreach ($this->variables as $varname => $value) {
            $$varname = $value;
        }
        ob_start();
        if ($config->show_template_names) {
//			echo '<div style="font-size: 10px; color: #FF0000;">&lt;['. $this->template . ']&gt;</div>';	
        }
        require $this->template;

        if ($config->show_template_names) {
//			echo '<div style="font-size: 10px; color: #FF0000;">&lt;/['. $this->template . ']&gt;</div>';	
        }

        $contentT .= ob_get_contents();
        ob_end_clean();

        if ($parseMetaPhp) {
            $contentT = str_replace(array("<![CPHP[", "]CPHP]>"), array("<?php ", "?>"), $contentT);
        }

        return $contentT;
    }

    public function checkboxGroup($instances, $selectedInstances = NULL) {
        global $subDir;
        $html = array();
//		$post = Post::$postVars;
        $intValueMethod = NULL;
        if ($instances instanceof DropdownAble) {
            $intValueMethod = $instances->getIntValueMethod();
        }

        foreach ($instances as $instance) {
            $checked = '';
            if (empty($intValueMethod)) {
                $value = $instance->getUid();
            } else {
                $value = $instance->$intValueMethod();
            }
            $name = dbTable::CC2U($instance->getClassName());
            if (!($selectedInstances === NULL )) {
                foreach ($selectedInstances as $selectedInstance) {
                    if ($selectedInstance->getUid() == $instance->getUid()) {
                        $checked = 'checked="checked"';
                        break;
                    }
                }
            }
            $label = $instance->getValue();
//			if(!empty($label)) {
            $html[] .= '&nbsp;' . $label . "</td>\n\t\t<td><input type=\"checkbox\" name=\"$name" . "[]\" value=\"$value\" $checked />";
//			}
        }
        $html = implode('</td></tr><tr><td>', $html);

        return "<table>\n\t<tr>\n\t\t<td>$html</td>\n\t</tr></table>";
    }

    protected function selectBox($varName, $className, $selected = NULL, $opts = '', $instances = NULL) {
        global $subDir;

        if (is_null($instances)) {
            $instances = new $className();
            $instances = $instances->lijst();
        }
        echo "<select name=\"$varName\" $opts >\n";

        if ($instances instanceof DropdownAble) {
            if (empty($selected)) {
                $selected = $instances->getDropdownDefaultValue();
            }
            $nullOption = (string) $instances->getDropdownNullOption();
            if (!empty($nullOption)) {
                echo "<option value=\"\" >$nullOption</option>\n";
            }
            $intValueMethod = $instances->getIntValueMethod();
        } else {
            echo "<option value=\"\" >-- alle --</option>\n";
            $intValueMethod = 'getUid';
        }

        if ($instances->isSelfReferring()) {
            echo $this->selfReferringList($className, null, TemplateEngine::OPTION_INDENTION, $selected);
            echo "</select>";
            return;
        }

        foreach ($instances as $instance) {
            $sel = "";
            if ($selected == $instance->$intValueMethod()) {
                $sel = ' selected="selected" ';
            }

            if (isset($_REQUEST["$varName"])) {
                if ($instance->$intValueMethod() == $_REQUEST["$varName"]) {
                    $sel = ' selected="selected" ';
                }
            }
            if (method_exists($instance, "getValue")) {
                echo "<option {$sel} value=\"{$instance->$intValueMethod()}\" >{$instance->getValue()}</option>\n";
            } else {
                echo "<option {$sel} value=\"{$instance->$intValueMethod()}\" >{$instance->$intValueMethod()}</option>\n";
            }
        }
        echo "</select>";
    }

    protected function selfReferringList($className, $instances = null, $indentionType = TemplateEngine::TABLE_INDENTION, $selected = null) {
        static $level = 0;
        $content = '';
        $indention = '?';
        switch ($indentionType) {
            case TemplateEngine::TABLE_INDENTION:
                $indention = '<td/>';
                break;
            case TemplateEngine::SPACE_INDENTION:
            case TemplateEngine::OPTION_INDENTION:
                $indention = '&nbsp;';
                break;
        }
        $level++;
        if (is_null($instances)) {
            // get top level
            $instances = new $className();
            $instances = $instances->lijst('SELECT * FROM ' . $instances->getTableName() . ' WHERE ISNULL(pid) ');
        }
        foreach ($instances as $instance) {
            $instance->getChildren();
            $prefix = str_repeat($indention, ($level - 1));
            switch ($indentionType) {
                case TemplateEngine::TABLE_INDENTION:
                    $content .= "<tr>$prefix<td><a href=\"/edit/$className/" . $instance->getUid() . "\">$instance</a></td></tr>\n";
                    break;
                case TemplateEngine::SPACE_INDENTION:
                    $content .= "$prefix<a href=\"/edit/$className/" . $instance->getUid() . "\">$instance</a>\n";
                    break;
                case TemplateEngine::OPTION_INDENTION:
                    $sel = '';
                    Logger::write("selected: $selected  instance->getUid(): {$instance->getUid()}");
                    if ($selected == $instance->getUid()) {
                        $sel = ' selected="selected" ';
                    }
                    $content .= "<option {$sel} value=\"{$instance->getUid()}\" >$prefix $prefix {$instance->getValue()}</option>\n";
                    break;
            }
            $content .= $this->selfReferringList($className, $instance, $indentionType, $selected);
        }
        $level--;
        return $content; // $inrichtingen;
    }

    /* 	private static function clearOffset($url)
      {
      //echo "$url<br/>";
      $sep ='?';
      $pos = strpos($url,$sep.'offset');
      if ($pos !== false)	$url=substr($url,0,$pos);
      //echo "$url<br/>";
      return $url;
      }
     */

    /**
     * create paginator
     *
     * @param int $total_rows	total number of records
     * @param string $base_url	url to page
     * @param int	 $max_rows	rows to show
     */
    public static function paginate($total_rows, $base_url, $max_rows) {
        $a = parse_url($base_url);
        $base_url = $a['path'];
        $offset = 0;
        if (isset($_GET['offset'])) {
            $offset = $_GET['offset'];
        }

        if (isset($_GET['rows'])) {
            $max_rows = $_GET['rows'];
        }
        self::$numPages = floor($total_rows / $max_rows);
        if ($total_rows % $max_rows)
            self::$numPages++;


        // PREV link
        if ($offset > 0) {
            self::disp_prev($limit, $base_url, $max_rows);
        }

        // Page Numbers
        if (self::$numPages > 1) {
            self::disp_page_nums($offset, $base_url, self::$numPages, $max_rows);
        }

        // Next link
        if (($offset + $max_rows) < $total_rows) {
            self::disp_next($offset, $base_url, $total_rows, $max_rows);
        }

        // Empty Results set 
        if ($total_rows == 0) {
            self::addToPaginator("<br><center>No Records to Display</center><p>");
        }
        // current page
        $currPage = (($offset / $max_rows) + 1);

        self::disp_paginator($currPage);
    }

    private static function disp_prev($offset, $base_url, $max_rows) {
        $offset = $_GET['offset'];

        $tmp_offset = ($offset - $max_rows);

        if ($tmp_offset < 0) {
            $tmp_offset = 0;
        }

        $offset = $tmp_offset;
        if (strstr($base_url, '?')) {
            $sep = '&';
        } else {
            $sep = '?';
        }
        $url = $base_url . $sep . 'offset=' . $offset;

        self::addToPaginator("<a href=" . $url . ">PREV</a> ");
    }

    private static function disp_next($offset, $base_url, $total_rows, $max_rows) {
        $tmp_offset = ($offset + $max_rows);
        //    echo"<p>tmp_offset=$tmp_offset<br>offset=$offset<p>";

        if ($tmp_offset > $total_rows) {
            // Nothing Happening Here
        } else {
            $offset = $tmp_offset;
            if (strstr($base_url, '?')) {
                $sep = '&';
            } else {
                $sep = '?';
            }
            $url = $base_url . $sep . 'offset=' . $offset;

            self::addToPaginator("<a href=" . $url . ">NEXT</a><br>");
        }
    }

    private static function disp_page_nums($offset, $base_url, $total_pages, $max_rows) {

        for ($i = 1; $i <= $total_pages; $i++) {
            $tmp_offset = (($i * $max_rows) - $max_rows);

//            $offset=$tmp_offset;
//            $base_url=self::clearOffset($base_url);
            if (strstr($base_url, '?')) {
                $sep = '&';
            } else {
                $sep = '?';
            }
            $url = $base_url . $sep . 'offset=' . $tmp_offset;

            self::$paginatorPages[$i] = $url;
        }
    }

    /**
     * generate paginator
     *
     * @param int $currPage	current page num
     */
    private static function disp_paginator($currPage) {
        // Edit this function to format the results as required
        $max = count(self::$paginatorPages);

        $split = "";
        $startHtml = "";
        $endHtml = "";

        $pages = array();


        foreach (self::$paginatorPages as $pageNum => $page) {

            if ($currPage != $pageNum) {

                $html = "<a href=" . $page . ">$pageNum</a>&nbsp;";
            } else {

                $html = "<b>$pageNum</b>&nbsp;";
            }

            if ($pageNum == 1 || $pageNum == $max) {
                self::$pages[$pageNum] = $html;
            }

            if ($pageNum >= $currPage - self::PAGINATOR_PAGES) {

                if ($pageNum <= ($currPage + self::PAGINATOR_PAGES)) {
                    self::$pages[$pageNum] = $html;
                } else {
                    if ($pageNum < ($currPage)) {
                        self::$pages[$pageNum] = $html;
                    }
                }
            }
        }

        $prev = 0;
        $pages = array();
        foreach (self::$pages as $pageNum => $page) {
            if (($pageNum - $prev - 1) > 0) {
                $pages[] = '...';
            }
            $pages[] = $page;
            $prev = $pageNum;
        }
//var_Export($pages);
        echo implode($pages);
    }

    private static function addToPaginator($html) {
        self::$paginatorHtml .= $html;
    }

}

?>