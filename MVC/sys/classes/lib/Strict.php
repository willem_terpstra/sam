<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Strict {
	
	function __get($name) {
		throw new StrictException("Variable $name not defined (get).");
	}

	function __set($name, $value) {
		throw new StrictException("Variable $name not defined (set).");
	}
}

?>
