<?
class Csv {
	
	const CSV_DIR = "csv/";
	
	public static function write($data) 
	{
        $datum = date("d-M-Y.G.i.s");

        $bestand = self::CSV_DIR .$datum. '.csv';
        $stream = fopen($bestand, 'a+');
        
		foreach ($data as $d) {
			fputcsv($stream, $d);
		};
		
        fclose($stream);
        
        return $datum. '.csv';;

    }
    
    public static function download($fileName)
    {	
		$f_location = self::CSV_DIR . $fileName;
		// required for IE, otherwise Content-disposition is ignored
		if(ini_get('zlib.output_compression'))
		  ini_set('zlib.output_compression', 'Off');
		
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Length: ' . filesize($f_location));
		header('Content-Disposition: attachment; filename=' . basename($fileName));
		readfile($f_location); 
  	}
}
?>