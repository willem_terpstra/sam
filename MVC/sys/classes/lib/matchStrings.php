<?php

	#================================================
	
	# private functions common to Red_Ws_Approx and Red_Ws_Eq
	
	#================================================
	
	# Find all matches to a given name
	class MatchStrings {
		private $path;
		private $stopwords;
		private $index = array();
		private $mismatch= -1;
//		const	MISMATCHTHRESHOLD = 3;
		private $mismatchThreshold = 3;
		
		private $haystackCount = 0;
		private $patternCount = 0;
		private $matchCount = 0;
		
	function __construct()
	{	// retrieve stopwords
		$this->path = $_SERVER['DOCUMENT_ROOT'] . "";
		$handle = fopen("{$this->path}/classes/matchStrings/stopwords.txt", "r");
		$contents = "";
		do {
		    $data = fread($handle, 8192);
		    if (strlen($data) == 0) {
		        break;
		    }
		    $contents .= $data;
		} while(true);
		fclose ($handle);
		$contents = strtoupper($contents);
		// stop-word list;
		$this->stopwords = explode ("\n",$contents);
	}
	
	public function FindMatchingNames (/* HashTable */ $words, /* String */ $name)
	{
		
		if (is_array($words))
		{	
			$wordsArr = $words;
			foreach ($wordsArr as $w)
			{	
			
				$this->FindMatchingNames($w,$name);
			}
			return;
		}
		
		if ($words . "" == "" || $name . "" == "" )
		{	throw new Exception ("name or words are empty");
			return array();
		}
		
		$this->mismatch = -1;
		$this->matchCount = 0;
		$this->wordCount = 0;
		$this->haystackCount = 0;
		$index = array();
		
		$this->AddIndex($index, $words) ;

	 	$normalizedName = $this->Normalize($name);

	 	$patternWordList = explode(" ",$normalizedName);
	 	$patternWordList = $this->handleInitials($patternWordList);
	 	
	 	

	 	Sort($patternWordList);
	 	
	 	while (	$patternWordList[0] . "" == "" )
	 	{	array_shift($patternWordList);
		}

		$this->patternCount = count($patternWordList);
	
	 	$candidates = $this->GetCandidates($index, $patternWordList);
	
	 	$matches =  array();
		$match = "";
		// FOR each wordlist wl in candidates{
	 	foreach ($candidates as $w1)
	 	{	// IF (SimilarWordByWord(w1, patternWordList))
			if ($this->SimilarWordByWord($w1, $patternWordList))
	  		{	// THEN matches <- append(matches,w1);
	  			$matches[] = $w1;
	  		}
	 	}
//echo "<br>haystackCount =>	{$this->haystackCount},
//	 patternCount	=>	{$this->patternCount},
//	 matchCount 	=>  {$this->matchCount}
//	 mismatch		=>  {$this->mismatch}<br/>";	 	

		return $matches;
//echo "<br/>";	
	}
	
	# Determine whether every word in one word list
	# matches some word in the other word list
	private function SimilarWordByWord(/* List */ $wordList1, /* List */ $wordList2)
	{
		$shortestWordList = (count($wordList1) <= count($wordList2)?$wordList1:$wordList2); //	<- shortest of wordList1 and wordList2
		$longestWordList = (count($wordList1) >= count($wordList2)?$wordList1:$wordList2);	//	<- longest of wordList1 and wordList2
		// FOR each word w1 in shortestWordList{
		foreach ($shortestWordList as $w1)
		{	//  LET w2 be the first element of longestWordList for which
		    foreach ($longestWordList as $w2)
			{	$this->Match($w1,$w2);
	            // IF (w2 == {})
	            if (is_null($w2))
	            {	//  THEN return failure;
	            	return false;
	            }
	        }
	  		// ELSE longestWordList <- longestWordList - w2;	???
		}
	
	 	return true;	// success; # every word has been matched
	
	}
	
	#=============================
	# Red_Ws_Approx
	#=============================
	
	# Add name to index
	
	private function AddIndex (/* HashTable */ &$index, /* String */ $name)
	{	
		$normalizedName = $this->Normalize($name);
	 	$wordList = explode(" ",$normalizedName);
	 	
	 	$wordList = $this->handleInitials($wordList);
	 	
	 	Sort($wordList);
 	
	 	while (	$wordList[0] . "" == "" )
	 	{	array_shift($wordList);
		}
		$this->haystackCount = count($wordList);
//echo count($wordList) . " words haystack<br/>";	

		
	
	
		foreach ($wordList as $word )
		{	if (isset($index[$this->soundexNl(strtoupper($word))]))
			{	$index[$this->soundexNl(strtoupper($word))][] = strtoupper($word);
			}
			else
			{	$index[$this->soundexNl(strtoupper($word))] = array(strtoupper($word));
			}
		}
	
	}
	
	# Find simular word lists
	
	private function GetCandidates (/* HashTable */ $index, /* List */ $patternWordList){
		
		$returnArr = array();
		foreach ($patternWordList as $pattern)
		{	$snd = $this->soundexNl($pattern);
			if (isset($index[$snd]))
			{	$returnArr[$snd] = $index[$snd];
			}
		}
		return $returnArr;
	
	}
	
	# Determine whether 2 words match
	private function Match ( $word1,  $word2){

		$mismatch = levenshtein  ($word1,$word2);
		if (	($mismatch  < $this->mismatchTreshold  ) 
	//			or
	//			(levenshtein (abb($word1),$word2) ) or
	//			(levenshtein ($word1, abb($word2)) ) or
	//			(levenshtein (abb($word1), abb($word2)) ) 
			)
		{	
//	echo "<br/>mismatch ($word1,$word2) == $mismatch<br/>";	
			$this->matchCount++;
			if ( $this->mismatch == -1 ) $this->mismatch = 0;
			$this->mismatch += $mismatch;	

			return true;
		}
//echo "<br/>nothing:	mismatch ($word1,$word2) == $mismatch<br/>";					
		return false;

	}
	
	# Remove irrelevant textual differences and stop words
	
	private function Normalize ( $name){
	
	 	//	Convert name to upper case;
	 	$name = strtoupper($name);
		
		// Replace punctuation other than period with a space;
		$name = str_replace(array(".",","),array(" "," "),$name );	// Delete periods;
		$name = trim($name);										// Remove leading, trailing,
		$name = str_replace ("\t", " ", $name );					// handle tabs
		 
		while(strpos($name, "  ")) 									//	and double spaces;
		{	$name = str_replace("  ", " ", $name); 
		}
		$name = $this->RemoveStopwords($name);
		
		return $name;
	}
	 
	private function RemoveStopwords ($name)
	{	
		//	Remove every word in name occurring in the stop-word list;
		foreach ( $this->stopwords as $stopword )
		{	$name = str_replace(" ".trim($stopword)." ","",$name );
		}
	
		return $name;
	
	}
	
	
	public function soundexNl($str)
	{	$str = strtoupper($str);
		$s = $str;
		$first = substr($str, 0, 1);
	
		$rest = substr($str,1,strlen($str) -1);
	
		$str = $rest;
		$orgSnd = $this->soundexReplace($s);
	
		$str = str_replace(array("A", "E", "H", "I", "O", "U", "J", "Y"),"", $str);
		$str = str_replace( 
			array( "QU","SCH","KS","KX","KC","CK","DT","TD","CH","SZ","IJ"),
			array( "KW","SEE","XX","XX","KK","KK","TT","TT","CG","SS","YY"), $str );
	
		$str = $first . $this->soundexReplace($str);
	
		return substr($str. "0000",0,4);
	
	}
	
	private function soundexReplace($str)
	{
		$str = str_replace (array("B","P"),1,$str );
		$str = str_replace (array("C", "G", "S", "K", "Z", "Q"),2,$str );
		$str = str_replace (array("D", "T"),3,$str );
		$str = str_replace (array("F", "V", "W"),4,$str );
		$str = str_replace (array("L"),5,$str );
		$str = str_replace (array("M", "N"),6,$str );
		$str = str_replace (array("R"),7,$str );
		$str = str_replace (array("X"),8,$str );
		
		
		return $str;
	
	}
	
	private function handleInitials ($wordList)
	{	// initials
		$initialsArr = array();
		$initials = "";

		for ($i = 0; $i < count($wordList);$i++)
		{	
			if (strlen($wordList[$i]) == 1)
			{	$initialsArr[] = $wordList[$i];
				$this->removeArrayElement($wordList,$i);
				$i--;
			}
		}
	
		foreach ($initialsArr as $initial)
		{	$initials .= $initial;
		}
		$wordList[] = $initials;

		return $wordList;
	}
	
	private function removeArrayElement(&$arr, $index)
	{    if(isset($arr[$index]))
		{   array_splice($arr, $index, 1);
    	}
	}
	
	public function setMismatchTreshold ( $mt )
	{	$this->mismatchTreshold = $mt;
	}
	
	public function getResult()
	{	return array (
			"haystackCount" =>	$this->haystackCount,
			"patternCount"	=>	$this->patternCount,
			"matchCount" 	=> $this->matchCount,
			"mismatch"		=> $this->mismatch
		);
		
	}
}

?>