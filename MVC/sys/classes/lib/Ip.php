<?php

class Ip {

	static public function get()
	{
		if (isset($_SERVER["HTTP_X_FORWARDED"]))
		{
			$ip = $_SERVER["HTTP_X_FORWARDED"];
		} else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	
	}

}
?>