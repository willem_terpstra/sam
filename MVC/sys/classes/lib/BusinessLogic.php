<?php
class BusinessLogic {
	
	static public function getAllowedStatii($className, $status) {
		if(empty($status)) {
			$status = 10; // default status  
		}
		$instance = new $className();
		$tableName = $instance->getTableName();
		$sql = 	'SELECT * FROM '. $tableName .'_status 
				WHERE uid IN (
					SELECT '. $tableName .'_status.uid
					FROM '. $tableName .'_status_direction
	    			INNER JOIN '. $tableName .'_status 
	        			ON ('. $tableName .'_status_direction.to_status_id = '. $tableName .'_status.uid)
					WHERE ('. $tableName .'_status_direction.from_status_id ='. $status .'))
				OR uid='. $status;

		$className = $className .'Status';
		$instance = new $className();
		$instances = $instance->lijst($sql);
		return $instances;
		
	}
	
	static public function getAllowedStatusChanges($className, $status) {
		if(empty($status)) {
			$status = 10; // default status  
		}
		
		$statusDirectionClass = $className .'StatusDirection';
		$statusDirectionInstances = new $statusDirectionClass();
		$statusDirectionInstances->loadByFromStatusId($status);
		
		
		$instance = new $className();
		$tableName = $instance->getTableName();
		$sql = 	'SELECT * FROM '. $tableName .'_status_direction 
				WHERE from_status_id='. $status; 
		$className = $className .'StatusDirection';
		$instance = new $className();
		$instances = $instance->lijst($sql);

		return $statusDirectionInstances;
		
	}
	

}
?>