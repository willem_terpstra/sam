<?php
// localhost:
// ABQIAAAAzcnB2Ep5-8RXXznENQhxgBT2yXp_ZAY8_ufC3CFXhHIE1NvwkxTE5ZUuQgPLbJaNK9MoYBockl2E8Q
class Location {
	private $testStr	= "joure nl";
	const GKEY	= "ABQIAAAAzcnB2Ep5-8RXXznENQhxgBT2yXp_ZAY8_ufC3CFXhHIE1NvwkxTE5ZUuQgPLbJaNK9MoYBockl2E8Q";
 	
	private $statusCode;
	private $messages = array();
	private $key = null;
	private $curlEnabled;
 	
	public function getMessages()
	{	return $this->messages;
	}
	
	public function __construct ()
	{	$this->curlEnabled = function_exists('curl_init');

		$this->key = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_wt_googlemaps_pi1.']['key'];
		if($_SERVER['HTTP_HOST'] == 'me') {
			$this->key = 'ABQIAAAAzcnB2Ep5-8RXXznENQhxgBQn_HSZNQry_34ozaWicKoNjA7r1xSCfLNZ26Pqhm0ux_9LsoT_BPVOTQ';
		} else {
			$this->key = 'ABQIAAAAzcnB2Ep5-8RXXznENQhxgBQOWrYCed5onUCMRfV2720c0tx6oBReIgmWeJtiDuLa988iYipdixqPQQ';
		}
	}
 	public function getCoordinates ($address )
 	{	
	 	
	 	$address = str_replace (array ("\r","\n")," ",$address);
	 	$address = urlencode($address);
	 	if ( is_null($this->key))
	 	{	Logger::write('No Google Geo key found...');
	 		throw new Exception ("No Google Geo key");
		}
	 	$geocodingUrl = "http://maps.google.com/maps/geo?q=".$address."&output=xml&key={$this->key}";
 	 	$headers = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/fileadmin/template/headers/GoogleGeo.txt" );
 	 	$headers = self::sanitizeHeaders($headers);
		$data = self::curlCall ( $geocodingUrl, $headers );

 		$xmlObj = simplexml_load_string ( $data );
		$this->statusCode = $xmlObj->Response->Status->code;
		if ($this->statusCode == 200 ) // ok 
	 	{	return $xmlObj->Response->Placemark->Point->coordinates;	 		
	 	}
	 	else
	 	{	switch ($this->statusCode)
	 		{	case 602 	: 	$this->messages[] = "Deze locatie kon niet gevonden worden door Google";
	 							break;
	 			default		:	$this->messages[] = "Fout in het bepalen van de locatie: {$this->statusCode}";
	 							break;
 			}
		}
		return false;
	}
 	
 	static private function sanitizeHeaders($headers)
	{	$headers = explode("\n",$headers);
		for ($i=0;$i<count($headers);$i++)
		{	if ($headers[$i] . "" != "" )
			{	$_headers[] = str_replace (array("\n","\r"),"",$headers[$i]);
			}
		}
		return $_headers;
	
	}
 	
 	static private function curlCall ($url, $headers )
 	{
 		if (!function_exists('curl_init')) return false;
 		
	 	$cookie_file_path = "{$_SERVER['DOCUMENT_ROOT']}/cookie.txt";
	 	
 		$ch = curl_init($url);
	 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 	// don't immediately print the data
		
		curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
		if (! is_null($headers))
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}	

//		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
//		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
	
		curl_setopt($ch,CURLOPT_TIMEOUT , 20 );
		$content = curl_exec($ch);
		return $content;	
	}
	
	static public function set_tile ( $lat, $lon, $uid )
	{
		$max_lat = 90.0;
		$min_lat =  0.0;
		$max_lon = 90.0;
		$min_lon =  0.0;
		$lower_tile = '';

		for ( $zoom = 1; $zoom <= 14; $zoom++ )
		{	$sql = "select lat,lon from zoom where zoom = " . $zoom;
			$rs = mysql_query ( $sql);
			//ExitSqlErr ( __FILE__, __LINE__, $sql );

			$ofs_lon = mysql_result( $rs,0,'lon'); // 0.002;
			$ofs_lat = mysql_result ( $rs,0,'lat'); // 0.00035;
			$lon_div = $max_lon - $min_lon;
			$lat_div = $max_lat - $min_lat; // 11482.86

			$rel_lat = $lat - $min_lat;
			$rel_lon = $lon - $min_lon;
			$lon_tile = floor($rel_lon / $ofs_lon);
			$lat_tile = floor($rel_lat / $ofs_lat);
			$lon_start = $min_lon + ( ($lon_tile) * $ofs_lon );
			$lat_start = $min_lat + ( ($lat_tile) * $ofs_lat );
			$tile_id = $lon_tile . "_" . $lat_tile;

			$sql = "insert ignore into tiles(place,lat,lon,zoom_id) values('" . $tile_id  . "','" . $lat_start . "','".$lon_start."','".$zoom."')";
			mysql_query ( $sql);
			$affected_rows = mysql_affected_rows();

			$sql = 'update ev_events set tile_id_'.$zoom.'="'.$tile_id.'" where uid='. $uid;
			mysql_query ( $sql);
//			ExitSqlErr ( __FILE__, __LINE__, $sql );
			$lower_tile = $tile_id;
		}

	}

}

?>