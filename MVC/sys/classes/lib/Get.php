<?php
class Get {

	public static $getVars = array();

	function __construct($url = NULL) {
		if(!empty($url)) {
			$q = parse_url($url);
			$gets = explode('&',$q['query']);
			$get = array();
			foreach($gets as $g) {
				$h = explode('=', $g);
				$get[$h[0]] = $h[1];
			}
			if(!empty($_GET)) {
				$get = array_merge($_GET, $get);
			}
		} else {
			$get = $_GET;
		}
		
		if(is_array($get)) {
			self::$getVars = $this->cleanData($get);
			unset ($_GET);
		}
	}

	private function cleanData($data) {
		if(is_array($data)) {
			foreach($data as $key => $value) {
				$data[$key] = $this->cleanData($value);
			}
			return $data;
		}
		$data = trim($data);
		$data = htmlentities($data);
		$data = mysql_real_escape_string($data);
		return $data;
	}
}
?>
