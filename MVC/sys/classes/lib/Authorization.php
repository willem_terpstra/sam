<?php

class Authorization {

    static private $checked = false;
    static private $user;

    /**
     * @deprecated
     */
    public static function Allowed($requiredAuth) {
        return self::getAuthLevel() >= $requiredAuth;
    }

    public static function ActionAllowed($action, /* ..View */ $view) {
        self::$checked = true;
        $currLevel = self::getAuthLevel();
        $vwClassAuth = new VwClassAuth();
        $instance = $view->getInstance();
        $vwClassAuth = $vwClassAuth->loadByName(get_class($instance));
        // check on failure
        $uid = $vwClassAuth->getUid();
        if (empty($uid)) {
            throw new SecurityException('No security defined for: ' . get_class($instance));
        }
        $securityArr = array(
            Controller::BROWSE => 'getBrowse',
            Controller::BROWSELIJST => 'getBrowse',
            Controller::CANCEL => 'getBrowse',
            Controller::COMPARE => 'getBrowse',
            Controller::CONFIRM => 'getBrowse',
            Controller::ADD => 'getUpdate',
            Controller::SAVENEW => 'getUpdate',
            Controller::DETAIL => 'getUpdate',
            Controller::EDITLIJST => 'getUpdate',
            Controller::IMAGE => 'getBrowse',
            Controller::LOGOUT => 'getBrowse',
            Controller::UPDATE => 'getUpdate',
            Controller::SEARCH => 'getBrowse',
            Controller::REMOVE => 'getUpdate',
            VehicleController::FIND_DIAGRAMS => 'getBrowse',
        );
        if (!array_key_exists($action, $securityArr)) {
            throw new SecurityException('Security not defined for action: ' . $action . ', class: ' . get_class($instance) . '.<br/>' . SecurityException::SECURITY_NOT_DEFINED . ' (instance of: ' . get_class($instance) . ')', SecurityException::SECURITY_NOT_DEFINED);
        }
        $authFunc = $securityArr[$action] . 'Auth';
        $auth = $vwClassAuth->$authFunc();
        $groupFunc = $securityArr[$action] . 'Group';
        $group = $vwClassAuth->$groupFunc();

        // Niet toegankelijk
        if ($auth == Auth::NOBODY) {
            throw new SecurityException('Deze pagina is niet toegankelijk', SecurityException::SECURITY_NOBODY);
        }

        $authFlag = false;
        switch ($auth) {
            case Auth::GAST:
                if (in_array($currLevel, array(Auth::GAST, Auth::USER, Auth::ADMIN, Auth::SUPER))) {
                    $authFlag = true;
                }
                break;
            case Auth::USER:
                if (in_array($currLevel, array(Auth::USER, Auth::ADMIN, Auth::SUPER))) {
                    $authFlag = true;
                }
                break;
            case Auth::ADMIN:
                if (in_array($currLevel, array(Auth::ADMIN, Auth::SUPER))) {
                    $authFlag = true;
                }
                break;
            default:
                throw new Exception("Security not implemented for action: $action, authorisation $auth and class " . get_class($instance));
        }
        if ($group == Groups::OWNER && $authFlag == true) {
            // Admin mag tot admin-owner
            if (in_array($currLevel, array(Auth::ADMIN, Auth::SUPER)) && in_array($auth, array(Auth::GAST, Auth::USER))) {
                return $authFlag;
            }
            // je moet eigenaar zijn om deze actie uit te mogen voeren
            if (empty($uid)) {
                // nieuw object is nog van niemand
                return $authFlag;
            }
            if (empty($_SESSION)) {
                return $authFlag; // geen controle op gebruiker mogelijk
            }
            if (!isset($_SESSION['user_id'])) {
                return $authFlag; // geen controle op gebruiker mogelijk
            }
            if (empty($_SESSION['user_id'])) {
                return $authFlag; // geen controle op gebruiker mogelijk
            }
            try {
                $usersId = $instance->getUsersId();
            } catch (dbTableException $e) {
                if ($e->getCode() == dbTableException::PROPERTY_DOES_NOT_EXIST) {
                    throw new SecurityException('Class ' . $instance->getClassName() . ' does not have a user reference, ownership cannot be determined.');
                }
                throw new Exception($e);
            }
            if ($usersId == $_SESSION['user_id']) {
                $authFlag = true;
            } else {
                $authFlag = false; // verkeerde gebruiker
            }
        }
        return $authFlag;
    }

    public function Check($userId, $passwd) {
        $passwd = mysql_real_escape_string($passwd);
        $auth = new VwLogin();
        $auth = $auth->loadByUserid($userId);
        $pwd = $auth->getPassw();
        if ($pwd == $passwd) {
            $_SESSION['mdUserid'] = md5($userId);
            $_SESSION['mdPasswd'] = md5($passwd);
            $user = new Users($auth->getUid());
            Unit::setUserUnits($user->getUnits());
            Languages::setLanguage($user->getLanguagesId());
//Klog::Loginfo('session: '. var_export($_SESSION, true));
            return true;
        } else {
            return false;
        }
    }

    public static function isLoggedIn() {
        return !empty($_SESSION['mdUserid']);
    }

    public static function logOut() {
        unset($_SESSION['mdUserid']);
        unset($_SESSION['mdPasswd']);
        self::$user = NULL;
//        if ($redirect) {
//            header('Location: /');
//        }
    }

    public static function setRequiredAuth($level) {
        self::$requiredAuth = $level;
    }

    public static function getAuthLevel() {
        if (empty($_SESSION['mdUserid'])) {
            return Auth::GAST;
        }
        if (empty($_SESSION['mdPasswd'])) {
            return Auth::GAST;
        }
        $vwLogin = new VwLogin();
        $vwLogin->loadByUseridMd($_SESSION['mdUserid']);
        $vwLogin = $vwLogin->first();
        if ($vwLogin->getPasswMd() == $_SESSION['mdPasswd']) {
            return $vwLogin->getAuthId();
        }
        return Auth::GAST;
    }

    /**
     *
     * @return Users
     */
    public static function getCurrentUser() {
        if (!empty(self::$user)) {
            return self::$user;
        }
        if (empty($_SESSION['mdUserid'])) {
            return NULL;
        }
        $vwLogin = new VwLogin();
        $vwLogin->loadByUseridMd($_SESSION['mdUserid']);
        $vwLogin = $vwLogin->first();
        $uid = $vwLogin->getUid();
        self::$user = new Users($uid);
        return self::$user;
    }

    static public function generatePassword($length = 8) {
        // start with a blank password
        $password = "";

        // define possible characters
        $possible = "0123456789bcdfghjkmnpqrstvwxyz";

        // set up a counter
        $i = 0;

        // add random characters to $password until $length is reached
        while ($i < $length) {
            // pick a random character from the possible ones
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);

            // we don't want this character if it's already in the password
            if (!strstr($password, $char)) {
                $password .= $char;
                $i++;
            }
        }

        // done!
        return $password;
    }

    public static function getChecked() {
        return self::$checked;
    }

}

?>