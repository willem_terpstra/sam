<h2>Klassen: <?= $racedag ?></h2>
<div>
    <a href="/browselijst/racedag">Terug naar alle evenementen</a>
</div>
<br/>
<div>
    <a href="/add/RacedagKlasse?racedag_id=<?= $racedag->getUid() ?>">Klasse toevoegen</a>
    &nbsp;
    &nbsp;
    <a href="/add_all_klasses/RacedagKlasse?racedag_id=<?= $racedag->getUid() ?>">Alle klassen toevoegen</a><br/>
</div>
<table class="table_1">
    <tr>
        <th>Edit</th>
        <th>Klasse</th>
    </tr>
    <?php
    $bgColorIndex = 0;
    foreach ($RacedagKlasse as $record) {
        try {
            $klasse = $record->getKlasse($record->getKlasseId());
        } catch (RacedagKlasseException $e) {
            continue;
        }
        ?>
        <tr class="<?php
    echo ($bgColorIndex % 2 ? 'clsEvenRow' : 'clsOddRow');
        ?>" >
            <td>
                <a href="/edit/RacedagKlasse/<?php echo $record->getUid() ?>">
                    <img src="/images/famfamfam/table_edit.png" />
                </a>
            </td>
            <td>
                <a href="/sessies/RacedagKlasse/<?php echo $record->getUid() ?>">
                    <?php
                    echo $klasse;
                    ?>
                </a>
            </td>

        </tr>
        <?php
        $bgColorIndex++;
    }
    ?>
</table>
