<?php
$bgColorIndex = 0;
$plaats = 1;
$fails = array();
?><br />
<hr />
<h2>Uitslag:</h2>
<br />
<table class="resultaat_table success">
    <tr>
        <th>Plaats</th>
        <th>Naam</th>
        <th>Straftijd sec.</th>
        <th>Rondes</th>
        <th>Rondes M1</th>
        <th>Rondes M2</th>
    </tr>
    <?php
    foreach ($VwUitslag as $record) {
        try {
            $m1 = $record->getRondesMancheOne();
            $m2 = $record->getRondesMancheTwo();
        } catch (VwUitslagException $e) {
            if ($e->getCode() == VwUitslagException::NOT_ENOUGH_LAPS || $e->getCode() == VwUitslagException::NO_LAPS) {
                $fails[] = $record;
                continue;
            }
        }
        ?>
        <tr class="<?= ($bgColorIndex % 2 ? 'clsEvenRow' : 'clsOddRow'); ?>" >
            <td style="width: 50px;" class="center bold"><?= $plaats ?></td>
            <td class="left"><?= $record->getResultaatNaam(); ?></td>
            <td class="right"><?= Resultaat::timeFormat($record->getStraf()) ?></td>
            <td class="center"><?= $record->getRondes(); ?>
            <td class="left" style="padding-left:20px"><?= $m1 ?>
            <td class="left" style="padding-left:20px"><?= $m2 ?>
            </td>
        </tr>
        <?php
        $bgColorIndex++;
        $plaats++;
    }
    ?>
</table>
<br />
<?php
if (!empty($fails)) {
    ?>
    <h2>DNF:</h2>
    <table class="resultaat_table fail">
        <tr>
            <th>Plaats</th>
            <th>Naam</th>
            <th>Straftijd sec.</th>
            <th>Rondes</th>
            <th>Rondes M1</th>
            <th>Rondes M2</th>
        </tr>
        <?php
        foreach ($fails as $record) {
            try {
                $m1 = $record->getRondesMancheOne(false);
            } catch (VwUitslagException $e) {
                if ($e->getCode() == VwUitslagException::NOT_ENOUGH_LAPS) {
                    
                } elseif ($e->getCode() == VwUitslagException::NO_LAPS) {
                    $m1 = 'n/a';
                }
            }
            try {
                $m2 = $record->getRondesMancheTwo(false);
            } catch (VwUitslagException $e) {
                if ($e->getCode() == VwUitslagException::NOT_ENOUGH_LAPS) {
                    
                } elseif ($e->getCode() == VwUitslagException::NO_LAPS) {
                    $m2 = 'n/a';
                }
            }
            ?>
            <tr class="<?= ($bgColorIndex % 2 ? 'clsEvenRow' : 'clsOddRow'); ?>" >
                <td style="width: 50px;" class="center bold"><?= $plaats ?></td>
                <td class="left"><?= $record->getResultaatNaam(); ?></td>
                <td class="right"><?= Resultaat::timeFormat($record->getStraf()) ?></td>
                <td class="center"><?= $record->getRondes(); ?>
                <td class="left" style="padding-left:20px"><?= $m1 ?>
                <td class="left" style="padding-left:20px"><?= $m2 ?>
                </td>
            </tr>
            <?php
            $bgColorIndex++;
            $plaats++;
        }
        ?>
    </table>
    <?php
}
?>
