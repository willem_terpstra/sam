<h2>SAM regelmatigheidsberekening</h2>
<a href="/add/Racedag">Evenement toevoegen</a>
<?php
if (count($Racedag) == 0) {
    ?>
    Nog geen racedagen ingevoerd
    <?php
    return;
}
?>
<style>
    .racedag_table tr td, table tr th {
        width: 120px;
    }
    .clsEvenRow {
        background-color: #CCCCCC;
    }
</style>
<script type="text/javascript">
    function showform(uid) {
        location.href = '/edit/Racedag/' + uid;
//        $.get('/showform.php?e=Racedag&id=' + uid, function(data) {
//            $('#racedag').html(data);
//        });
    }
</script>
<table class="racedag_table">
    <tr>
        <th>Edit</th>
        <th>Evenement</th>
        <th>Datum</th>
    </tr>
    <?php
    $bgColorIndex = 0;
    foreach ($Racedag as $record) {
        ?>
        <tr class="<?php
    echo ($bgColorIndex % 2 ? 'clsEvenRow' : 'clsOddRow');
        ?>" >
            <td>
                <img
                    onclick="showform(<?php echo $record->getUid() ?>)"
                    src="/images/famfamfam/table_edit.png" 
                    />
            </td>
            <td>
                <a href="/browselijst/import?racedag_id=<?php echo $record->getUid() ?>">
                    <?php
                    echo $record->getNaam();
                    ?>
                </a>
            </td>
            <td style="text-align: center;width:250px; color: blue; text-decoration: underline; cursor: pointer;">
                <a href="/browselijst/import?racedag_id=<?php echo $record->getUid() ?>">
                    <?php
                    echo $record->getDatum();
                    ?>
                </a>
            </td>

        </tr>
        <?php
        $bgColorIndex++;
    }
    ?>
</table>

