<h2>Klasse toevoegen aan: <?= $racedag ?></h2>
<?php
$racedagParam = '';
if ($racedag->getUid()) {
    $racedagParam = "?racedag_id=" . $racedag->getUid();
}
?>
<form 
    id="frmRacedagKlasse" 
    name="RacedagKlasse" 
    method="POST" 
    action="/update/RacedagKlasse<?= $racedagParam ?>"
    >
    <input type="hidden" id="act" name="act" value="" />
<!-- 			<input type="hidden" name="uid" value="<?php echo $record->getUid(); ?>"  /> -->
    <input type="hidden" 
           name="uid" 
           id="uid"
           value="<?php echo $record->getUid(); ?>" 
           />
    <input type="hidden" 
           name="racedag_id" 
           id="racedag_id"
           value="<?php echo $racedag->getUid() ?>" 
           />
    <fieldset>
        <legend>RacedagKlasse</legend>
        <table>
            <?php
            if (isset($messages)) {
                echo '<div class="clsError">';
                foreach ($messages as $message) {
                    echo $message . '<br/>';
                }
                echo '</div>';
            }
            ?>				
            <tr>
                <td valign="top">
                    <label for="klasse_id">Klasse</label>

                </td>
                <td>
                    <?php
                    $this->selectBox("klasse_id", "Klasse", $record->getKlasseId());
                    ?>
                </td>
            </tr>				

        </table>
    </fieldset>

    <button
        onclick="
            frm = document.getElementById('frmRacedagKlasse');
            el_act = document.getElementById('act');
            el_act.value = 'SAVE';
            frm.submit();
        "
        type="button">Save</button>
    <button
        onclick="
            frm = document.getElementById('frmRacedagKlasse');
            el_act = document.getElementById('act');
            el_act.value = 'REMOVE';
            frm.submit();
        "
        type="button">Delete</button>
    <button
        onclick="
            frm = document.getElementById('frmRacedagKlasse');
            el_act = document.getElementById('act');
            el_act.value = 'CANCEL';
            frm.submit();
        "
        type="button">Cancel</button>
</form>