<table>
<tr>
    <th>Bestand</th>
    <th>Status</th>
    <th>Resultaat</th>
</tr>
<?php
$bgColorIndex = 0;
$uid = NULL;
foreach ($ImportFile as $record) {
    $uid = $record->getUid();
    ?>
    <tr class="<?php
    echo ($bgColorIndex % 2 ? 'clsEvenRow' : 'clsOddRow');
    ?>" >
        <td style="width:250px; color: blue; text-decoration: underline; cursor: pointer;">
            <div id="<?= $record->getUid() ?>"
                 onclick="showform(<?= $record->getUid() ?>);"
                 >
                     <?php
                     echo $record->getName();
                     ?>
            </div>
        </td>

        <td style="text-align: center;">
            <?= $record->getStatus() ?>
        </td>
        <td style="text-align: center; text-decoration: underline; cursor: pointer; color: blue; font-weight: bold">
            <div id="resultaat_<?= $record->getUid() ?>"
                 onclick="resultaat(<?= $record->getUid() ?>)">
                     <?php echo $record->getResultaatAantal() . ' rijders'; ?>
            </div>
        </td>

    </tr>
    <?php
    $bgColorIndex++;
}
    
?>
</table>
<?php 
if($uid) {
?>
<script>
    resultaat(<?= $uid ?>);
</script>
<?php 
}
?>