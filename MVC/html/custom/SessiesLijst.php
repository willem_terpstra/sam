    <h2>
        <?= $racedagKlasse ?>
    </h2>
<div class="noPrint">
    <div>
        <a href="/browselijst/import?racedag_id=<?= $racedagKlasse->getRacedagId() ?>" ?>Terug naar alle klassen</a>
    </div>
    <br/>
    <div>
        <a href="/add/Sessies?racedag_klasse_id=<?= $racedagKlasse->getUid() ?>">Sessie toevoegen</a>
        &nbsp;
        &nbsp;
        <a href="/add_all_sessies/Sessies?racedag_klasse_id=<?= $racedagKlasse->getUid() ?>">Alle sessies toevoegen</a>
    </div>
    <table class="table_1">
        <tr>
            <th>Edit</th>
            <th>Sessie</th>
        </tr>
        <?php
        $bgColorIndex = 0;
        foreach ($Sessies as $record) {
            ?>
            <tr class="<?php
        echo ($bgColorIndex % 2 ? 'clsEvenRow' : 'clsOddRow');
            ?>" >
                <td>
                    <a href="/edit/Sessies/<?php echo $record->getUid() ?>?racedag_klasse_id=<?= $record->getRacedagKlasseId() ?>">
                        <img src="/images/famfamfam/table_edit.png" />
                    </a>
                </td>
                <td>
                    <a href="/browse/Sessies/<?php echo $record->getUid() ?>">
                        <?php
                        echo $record->getSessieNaam($record->getSessieNaamId());
                        ?>
                    </a>
                </td>

            </tr>
            <?php
            $bgColorIndex++;
        }
        ?>
    </table>
</div>