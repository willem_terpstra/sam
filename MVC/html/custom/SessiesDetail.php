<h2>Sessie: 
    <?= $racedagKlasse ?>
</h2>
<form 
    id="frmSessies" 
    name="Sessies" 
    method="POST" 
    action="/update/sessies"
    >
    <input type="hidden" 
           name="uid" 
           id="uid"
           value="<?php echo $record->getUid(); ?>" 
           />
    <input type="hidden" 
           name="racedag_klasse_id" 
           id="racedag_klasse_id"
           value="<?php echo $racedagKlasse->getUid(); ?>" 
           />
    <input type="hidden" id="act" name="act" value="" />
<!-- 			<input type="hidden" name="uid" value="<?php echo $record->getUid(); ?>"  /> -->
    <?php
    if (isset($messages)) {
        echo '<div class="clsError">';
        foreach ($messages as $message) {
            echo $message . '<br/>';
        }
        echo '</div>';
    }
    ?>				

    <table>
        <tr>
            <td valign="top">
                <label for="sessie_naam_id">
                    naam
                </label>

            </td>
            <td>
                <?php $this->selectBox("sessie_naam_id", "SessieNaam", $record->getSessieNaamId()); ?>
                <br/>		
            </td>
        </tr>				
    </table>

    <button
        onclick="
            frm = document.getElementById('frmSessies');
            el_act = document.getElementById('act');
            el_act.value = 'SAVE';
            frm.submit();
        "
        type="button">Save</button>
        <?php
        if ($record->getUid()) {
            ?>
        <button
            onclick="
                frm = document.getElementById('frmSessies');
                el_act = document.getElementById('act');
                el_act.value = 'REMOVE';
                frm.submit();
            "
            type="button">Delete</button>
            <?php
        }
        ?>
    <button
        onclick="
            location.href = '/sessies/RacedagKlasse/<?= $racedagKlasse->getUid(0) ?>'
        "
        type="button">Cancel</button>
</form>