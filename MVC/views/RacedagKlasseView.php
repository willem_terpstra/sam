<?php

class RacedagKlasseView extends _RacedagKlasseView {

    protected $messages = array();

    function __construct($uid = NULL) {
        try {
            $this->RacedagKlasse = new RacedagKlasse($uid);
        } catch (dbTableException $e) {
            $this->RacedagKlasse = new RacedagKlasse();
            if ($e->getCode() == dbTableException::LOAD_FAILED_EXCEPTION)
                $this->setMessages('Klasse bestaat niet meer voor dit evenement');
        }
    }

    public function detail() {
        $racedag = NULL;
        if ($this->RacedagKlasse->getUid()) {
            $racedag = new Racedag($this->RacedagKlasse->getRacedagId());
        } else {
            if (isset($_GET) && array_key_exists('racedag_id', $_GET))
                $racedag = new Racedag($_GET['racedag_id']);
            else {
                if (array_key_exists('racedag_id', Post::$postVars)) {
                    $racedag = new Racedag($post = Post::$postVars['racedag_id']);
                }
            }
        }
        if (!$racedag) {
            return 'Evenement niet gevonden';
        }
        $messages = array_merge($this->getMessages(), $this->RacedagKlasse->getMessages());
        $t = new TemplateEngine("/MVC/html/custom/RacedagKlasseDetail.php");
        $t->assign('racedag', $racedag);
        $t->assign('messages', $messages);
        $t->assign("record", $this->RacedagKlasse);

        return $t->execTemplate();
    }

    public function save() {
        $post = Post::$postVars;

        $this->RacedagKlasse->setRacedagId($post['racedag_id']);
        $this->RacedagKlasse->setKlasseId($post['klasse_id']);

        try {
            $this->RacedagKlasse->saveOrUpdate();
        } catch (dbTableException $e) {
            $this->RacedagKlasse->setMessage($e->getMessage());
            return $this->detail();
        }
        header('Location: /browselijst/import?racedag_id=' . $post['racedag_id']);
        exit;
    }

    public function showSessies() {

//        $klasseTotaal = new VwKlasseTotalenView();
//        $tot = NULL;
//        try {
//            $tot = $klasseTotaal->getKlasseTotaal($this->RacedagKlasse->getUid());
//        } catch (VwKlasseTotalenException $e) {
//            $tot = '';
//        }
        $vwUitslag = new VwUitslagView();
        $tot = $vwUitslag->browseLijst("SELECT * FROM vw_uitslag WHERE racedag_klasse_id={$this->RacedagKlasse->getUid()}");

        $sql = "SELECT * FROM sessies WHERE racedag_klasse_id={$this->RacedagKlasse->getUid()}";
        $sessies = new SessiesView();
        $sessies->setRaceDagKlasse($this->RacedagKlasse);
        return $sessies->lijst($sql) . $tot;
    }

    public function remove() {
        try {
            $this->RacedagKlasse->remove();
        } catch (dbTableException $e) {
            switch ($e->getCode()) {
                case dbTableException::CANNOT_UPDATE_PARENT_ROW:
                case dbTableException::CANNOT_DELETE_OR_UPDATE_PARENT_ROW:
                    $this->setMessages('Er zijn sessies gekoppeld aan deze klasse, verwijder die eerst');
                    return $this->detail();
            }
        }

        header('Location: /browselijst/import?racedag_id=' . Post::$postVars['racedag_id']);
        exit;
    }

    public function addAllKlasses() {
//        return __CLASS__ . ' ' . __FUNCTION__;
        $klasses = new Klasse();
        $klassesLijst = $klasses->lijst();
        $racedagId = $_GET['racedag_id'];
        foreach ($klassesLijst as $klasseInstance) {
            $racedagKlasse = new RacedagKlasse();
            $racedagKlasse->setRacedagId($racedagId);
            $racedagKlasse->setKlasseId($klasseInstance->getUid());
            try {
                $racedagKlasse->saveOrUpdate();
            } catch (dbTableException $e) {
                if ($e->getCode() == dbTableException::DUPLICATE_ENTRY) {
//                    $this->setMessage();
                } else {
                    throw new dbTableException($e->getMessage(), $e->getCode());
                }
            }
        }
        header('Location: /browselijst/import?racedag_id=' . $racedagId);
    }

}

