<?php

class VwUitslagView extends _VwUitslagView {

    public function browseLijst($sql = null) {
        $VwUitslag = new VwUitslag();
        $VwUitslag = $VwUitslag->lijst($sql);
        $t = new TemplateEngine("/MVC/html/custom/browseVwUitslagLijst.php");
        $t->assign("VwUitslag", $VwUitslag);
        return $t->execTemplate();
    }

}
