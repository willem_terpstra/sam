<?php

class ResultaatView extends _ResultaatView {
private $success;
private $title;
    public function failLijst($sql = null) {
        $this->title = 'DNF:';
        $this->success = false;
        return $this->browseLijst($sql);
    }

    public function successLijst($sql = null) {
        $this->success = true;
        $this->title = 'Uitslag:';
        return $this->browseLijst($sql);
    }

    public function browseLijst($sql = null) {
        $Resultaat = new Resultaat();
        $Resultaat = $Resultaat->lijst($sql);
        $t = new TemplateEngine("/MVC/html/custom/browseResultaatLijst.html");
        $t->assign("success", $this->success);
        $t->assign("title", $this->title);
        $t->assign("Resultaat", $Resultaat);
        return $t->execTemplate();
    }

}

?>