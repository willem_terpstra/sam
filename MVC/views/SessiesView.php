<?php

class SessiesView extends _SessiesView {

    private $racedagKlasse = NULL;
    protected $messages = array();

    function __construct($uid = NULL) {
        try {
            $this->Sessies = new Sessies($uid);
        } catch (dbTableException $e) {
            $this->Sessies = new Sessies();
            if ($e->getCode() == dbTableException::LOAD_FAILED_EXCEPTION)
                $this->setMessages('Sessie bestaat niet meer');
        }
    }

    public function setRaceDagKlasse(RacedagKlasse $racedagKlasse) {
        $this->racedagKlasse = $racedagKlasse;
    }

    public function detail() {
        $post = Post::$postVars;
        if (array_key_exists('racedag_klasse_id', $post)) {
            $this->racedagKlasse = new RacedagKlasse($post['racedag_klasse_id']);
        } else {
            $this->racedagKlasse = new RacedagKlasse($_GET['racedag_klasse_id']);
        }
        $messages = array_merge($this->getMessages(), $this->Sessies->getMessages());
        $t = new TemplateEngine("/MVC/html/custom/SessiesDetail.php");
        $t->assign('messages', $messages);
        $t->assign('racedagKlasse', $this->racedagKlasse);
        $t->assign("record", $this->Sessies);

        return $t->execTemplate();
    }

    public function lijst($sql = null) {
        $Sessies = new Sessies();
        $Sessies = $Sessies->lijst($sql);
        $t = new TemplateEngine("/MVC/html/custom/SessiesLijst.php");
        $t->assign('racedagKlasse', $this->racedagKlasse);
        $t->assign("Sessies", $Sessies);
        return $t->execTemplate();
    }

    public function save() {
        $post = Post::$postVars;
        $this->Sessies->setRacedagKlasseId($post['racedag_klasse_id']);
        $this->Sessies->setSessieNaamId($post['sessie_naam_id']);

        try {
            $this->Sessies->saveOrUpdate();
        } catch (dbTableException $e) {
            $this->Sessies->setMessage($e->getMessage());
            return $this->detail();
        }
        header('Location: /sessies/RacedagKlasse/' . $post['racedag_klasse_id']);
        exit;
    }

    public function remove() {
        $racedagKlasseId = $this->Sessies->getRacedagKlasseId();
        if(empty($racedagKlasseId)) {
            $p = Post::$postVars;
            if(array_key_exists('racedag_klasse_id', $p)) {
                $racedagKlasseId = $p['racedag_klasse_id'];
            }
        }
        $this->Sessies->remove();
        header('Location: /sessies/RacedagKlasse/' . $racedagKlasseId);
        exit;
    }

    public function browseDetail() {
//        $t = new TemplateEngine("browseSessiesDetail.html");
//        $t->assign("record", $this->Sessies);
//
//        return $t->execTemplate();
//        $racedagKlasse = new RacedagKlasse($_GET['racedag_klasse_id']);
//        $racedagId = $racedagKlasse->getRacedagId();
        $importFiles = new ImportFileView();
        $importFiles->setSessiesId($this->Sessies->getUid());
        $files = $importFiles->browseLijst("SELECT * FROM import_file WHERE sessies_id={$this->Sessies->getUid()}");

//        $importFileId = NULL;
//        if (array_key_exists('uid', $_GET)) {
//            $importFileId = $_GET['uid'];
//        }
//        $currentImportFile = new ImportFile($importFileId);

        $t = new TemplateEngine("/MVC/html/custom/browseImportLijst.html");
        $t->assign('sessie', $this->Sessies);
//        $t->assign("Import", $Import->lijst($sql));
//        $t->assign("record", $currentImportFile);
        $t->assign('files', $files);
        return $t->execTemplate();
    }

    public function addAllSessies() {
//        return __CLASS__ . ' ' . __FUNCTION__;
        $sessieNaam = new SessieNaam();
        $sessieNaamLijst = $sessieNaam->lijst();
        $racedagKlasseId = $_GET['racedag_klasse_id'];
        
        foreach ($sessieNaamLijst as $sessieNaamInstance) {
            $sessie = new Sessies();
            $sessie->setRacedagKlasseId($racedagKlasseId);
            $sessie->setSessieNaamId($sessieNaamInstance->getUid());
            try {
                $sessie->saveOrUpdate();
            } catch (dbTableException $e) {
                if ($e->getCode() == dbTableException::DUPLICATE_ENTRY) {
//                    $this->setMessage();
                } else {
                    throw new dbTableException($e->getMessage(), $e->getCode());
                }
            }
        }
        header('Location: /sessies/RacedagKlasse/' . $racedagKlasseId);
    }

}
