<?php

class ImportFileView extends _ImportFileView {

    public function getImportFile() {
        return $this->ImportFile;
    }

    public function setSessiesId($sessiesId) {
        $this->ImportFile->setSessiesId($sessiesId);
    }

    public function browseLijst($sql = null) {
        $sessiesId = $this->ImportFile->getSessiesId();
        $uid = NULL;
        if ($sessiesId)
            $uid = sqlGet('SELECT uid FROM import_file WHERE sessies_id=' . $sessiesId);
        if (empty($uid))
            $uid = false;
        $t = new TemplateEngine("/MVC/html/custom/browseImportFileLijst.html");
        $t->assign('uid', $uid);
        $html = $this->getFileRows($sql);
        $t->assign('importFileRows', $html);
        return $t->execTemplate();
    }

    public function getFileRows($sql = NULL) {
        $ImportFile = new ImportFile();

        $t = new TemplateEngine("/MVC/html/custom/ImportFileLijstRows.php");
        if ($sql)
            $t->assign("ImportFile", $ImportFile->lijst($sql));
        else
            $t->assign('ImportFile', $ImportFile->lijst('SELECT * FROM import_file WHERE uid = ' . $this->ImportFile->getUid()));
//            $t->assign('ImportFile', $ImportFile->lijst());
        return $t->execTemplate();
    }

    public function detail() {
        $t = new TemplateEngine("/MVC/html/custom/ImportFileDetail.html");
        $t->assign("record", $this->ImportFile);
        return $t->execTemplate();
    }

    public function getHead() {
        // sessiename & klasse
        $text = $this->ImportFile->getSessies()->getSessieNaam()->getNaam() . ' ' . $this->ImportFile->getSessies()->getRacedagKlasse()->getKlasse()->getNaam();
        return "<h1>$text</h1>";
    }

    public function showResult() {
        $minRondenPerc = $this->ImportFile->getSessies()->getRacedagKlasse()->getRacedag()->getMinRondenPerc();
        $minRondenPerc /= 100;
        $requiredLaps = sqlGet("SELECT MAX(rondes) * $minRondenPerc AS max_rondes FROM resultaat WHERE import_file_id=" . $this->ImportFile->getUid());
        if (empty($requiredLaps)) {
            return 'Geen resultaten gevonden, klik eerst op \'Verwerk\'';
        }
//        $head = $this->getHead();
        $resultaatView = new ResultaatView();
        $success = $resultaatView->successLijst('SELECT * FROM resultaat WHERE import_file_id=' . $this->ImportFile->getUid() . " AND rondes >= $requiredLaps " . ' ORDER BY msec_straf ASC ');
        $fail = $resultaatView->failLijst('SELECT * FROM resultaat WHERE import_file_id=' . $this->ImportFile->getUid() . " AND rondes < $requiredLaps " . ' ORDER BY msec_straf ASC ');
        return $success . $fail;
    }

    public function save() {
        $post = Post::$postVars;
        unset($post['act']);
        $this->ImportFile->fill($post);
        try {
            $this->ImportFile->saveOrUpdate();
        } catch (dbTableException $e) {
            $this->ImportFile->setMessage($e->getMessage());
            return $this->detail();
        }
        return $this->quit();
    }

    public function remove() {
        $sessiesId = $this->ImportFile->getSessiesId();
        $resultaat = new Resultaat();
        foreach ($resultaat->lijst('SELECT * FROM resultaat WHERE import_file_id=' . $this->ImportFile->getUid()) as $r) {
            $r->remove();
        }
        $this->ImportFile->remove();
    }

}
