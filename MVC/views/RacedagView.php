<?php

class RacedagView extends _RacedagView {


    public function lijst($sql = null) {
        $Racedag = new Racedag();
        $Racedag = $Racedag->lijst($sql);
        $t = new TemplateEngine("/MVC/html/custom/browseRacedagLijst.php");
        $t->assign("Racedag", $Racedag);
        return $t->execTemplate();
    }

    public function browseLijst($sql = null) {

        $racedag = new Racedag();
        $racedagLijst = $racedag->lijst($sql);
        $t = new TemplateEngine("/MVC/html/custom/browseRacedagLijst.php");
        $t->assign("Racedag", $racedagLijst);
        $racedagLijstHtml = $t->execTemplate();
        return $racedagLijstHtml;
    }

    public function detail() {
        $messages = $this->getMessages();
        $t = new TemplateEngine("/MVC/html/custom/RacedagDetail.html");
        if (!empty($messages)) {
            $t->assign('messages', $messages);
        }
        $t->assign("record", $this->Racedag);
        return '<div id="racedag">' . $t->execTemplate() . '</div>';
    }

    public function save() {
        $post = Post::$postVars;
        unset($post['act']);
        if (empty($post['uid'])) {
            unset($post['uid']);
        }
//        $post['datum'] = str_replace('-', '', $post['datum']);
        $this->Racedag->fill($post);
        try {
            $this->Racedag->saveOrUpdate();
        } catch (dbTableException $e) {
            $this->Racedag->setMessage($e->getMessage());
            return $this->detail();
        }
        return $this->quit();
    }

    public function delete() {
        $this->Racedag->removeChildren();
        return $this->remove();
    }

    public function remove() {
        try {
            $this->Racedag->remove();
        } catch (dbTableException $e) {
            switch ($e->getCode()) {
                case dbTableException::CANNOT_UPDATE_PARENT_ROW:
                case dbTableException::CANNOT_DELETE_OR_UPDATE_PARENT_ROW:
                    $this->setMessages('Er zijn gegevens gekoppeld aan deze racedag, verwijder die eerst: <a href="/delete/Racedag/' . $this->Racedag->getUid() . '">Rucksichtslos verwijderen</a>');
                    return;
            }
            throw new dbTableException($e->getMessage(), $e->getCode());
        }
        return $this->browseLijst();
    }

}

?>