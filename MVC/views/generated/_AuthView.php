<?php

class _AuthView extends View {	
	/**
	 *
	 * @var type Auth	 
	 */
	
	protected $Auth;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->Auth = new Auth($uid);
	}
	
	public function getInstance() {
		return $this->Auth;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getAuth () {
		return $this->Auth;
	}
	
	public function lijst($sql = null)	{	
		$Auth = new Auth();
		$Auth = $Auth->lijst($sql);
		$t = new TemplateEngine("AuthLijst.html");
		$t->assign("Auth",$Auth);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$Auth = new Auth();
		$Auth = $Auth->lijst($sql);
		$t = new TemplateEngine("browseAuthLijst.html");
		$t->assign("Auth",$Auth);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->Auth->getMessages());
		$t = new TemplateEngine("AuthDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Auth);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseAuthDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Auth);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->Auth->validate($post);
		$m = $this->Auth->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->Auth->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->Auth->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->Auth->remove();
		return $this->quit();
	}
}

?>