<?php

class _SessieNaamView extends View {	
	/**
	 *
	 * @var type SessieNaam	 
	 */
	
	protected $SessieNaam;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->SessieNaam = new SessieNaam($uid);
	}
	
	public function getInstance() {
		return $this->SessieNaam;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getSessieNaam () {
		return $this->SessieNaam;
	}
	
	public function lijst($sql = null)	{	
		$SessieNaam = new SessieNaam();
		$SessieNaam = $SessieNaam->lijst($sql);
		$t = new TemplateEngine("SessieNaamLijst.html");
		$t->assign("SessieNaam",$SessieNaam);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$SessieNaam = new SessieNaam();
		$SessieNaam = $SessieNaam->lijst($sql);
		$t = new TemplateEngine("browseSessieNaamLijst.html");
		$t->assign("SessieNaam",$SessieNaam);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->SessieNaam->getMessages());
		$t = new TemplateEngine("SessieNaamDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->SessieNaam);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseSessieNaamDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->SessieNaam);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->SessieNaam->validate($post);
		$m = $this->SessieNaam->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->SessieNaam->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->SessieNaam->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->SessieNaam->remove();
		return $this->quit();
	}
}

?>