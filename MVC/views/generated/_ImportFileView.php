<?php

class _ImportFileView extends View {	
	/**
	 *
	 * @var type ImportFile	 
	 */
	
	protected $ImportFile;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->ImportFile = new ImportFile($uid);
	}
	
	public function getInstance() {
		return $this->ImportFile;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getImportFile () {
		return $this->ImportFile;
	}
	
	public function lijst($sql = null)	{	
		$ImportFile = new ImportFile();
		$ImportFile = $ImportFile->lijst($sql);
		$t = new TemplateEngine("ImportFileLijst.html");
		$t->assign("ImportFile",$ImportFile);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$ImportFile = new ImportFile();
		$ImportFile = $ImportFile->lijst($sql);
		$t = new TemplateEngine("browseImportFileLijst.html");
		$t->assign("ImportFile",$ImportFile);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->ImportFile->getMessages());
		$t = new TemplateEngine("ImportFileDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->ImportFile);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseImportFileDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->ImportFile);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->ImportFile->validate($post);
		$m = $this->ImportFile->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->ImportFile->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->ImportFile->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->ImportFile->remove();
		return $this->quit();
	}
}

?>