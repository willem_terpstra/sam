<?php

class _VwUitslagView extends View {	
	/**
	 *
	 * @var type VwUitslag	 
	 */
	
	protected $VwUitslag;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->VwUitslag = new VwUitslag($uid);
	}
	
	public function getInstance() {
		return $this->VwUitslag;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getVwUitslag () {
		return $this->VwUitslag;
	}
	
	public function lijst($sql = null)	{	
		$VwUitslag = new VwUitslag();
		$VwUitslag = $VwUitslag->lijst($sql);
		$t = new TemplateEngine("VwUitslagLijst.html");
		$t->assign("VwUitslag",$VwUitslag);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$VwUitslag = new VwUitslag();
		$VwUitslag = $VwUitslag->lijst($sql);
		$t = new TemplateEngine("browseVwUitslagLijst.html");
		$t->assign("VwUitslag",$VwUitslag);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->VwUitslag->getMessages());
		$t = new TemplateEngine("VwUitslagDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->VwUitslag);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseVwUitslagDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->VwUitslag);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->VwUitslag->validate($post);
		$m = $this->VwUitslag->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->VwUitslag->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->VwUitslag->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->VwUitslag->remove();
		return $this->quit();
	}
}

?>