<?php

class _VwKlasseTotalenView extends View {	
	/**
	 *
	 * @var type VwKlasseTotalen	 
	 */
	
	protected $VwKlasseTotalen;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->VwKlasseTotalen = new VwKlasseTotalen($uid);
	}
	
	public function getInstance() {
		return $this->VwKlasseTotalen;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getVwKlasseTotalen () {
		return $this->VwKlasseTotalen;
	}
	
	public function lijst($sql = null)	{	
		$VwKlasseTotalen = new VwKlasseTotalen();
		$VwKlasseTotalen = $VwKlasseTotalen->lijst($sql);
		$t = new TemplateEngine("VwKlasseTotalenLijst.html");
		$t->assign("VwKlasseTotalen",$VwKlasseTotalen);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$VwKlasseTotalen = new VwKlasseTotalen();
		$VwKlasseTotalen = $VwKlasseTotalen->lijst($sql);
		$t = new TemplateEngine("browseVwKlasseTotalenLijst.html");
		$t->assign("VwKlasseTotalen",$VwKlasseTotalen);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->VwKlasseTotalen->getMessages());
		$t = new TemplateEngine("VwKlasseTotalenDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->VwKlasseTotalen);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseVwKlasseTotalenDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->VwKlasseTotalen);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->VwKlasseTotalen->validate($post);
		$m = $this->VwKlasseTotalen->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->VwKlasseTotalen->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->VwKlasseTotalen->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->VwKlasseTotalen->remove();
		return $this->quit();
	}
}

?>