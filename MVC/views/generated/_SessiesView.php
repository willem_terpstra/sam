<?php

class _SessiesView extends View {	
	/**
	 *
	 * @var type Sessies	 
	 */
	
	protected $Sessies;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->Sessies = new Sessies($uid);
	}
	
	public function getInstance() {
		return $this->Sessies;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getSessies () {
		return $this->Sessies;
	}
	
	public function lijst($sql = null)	{	
		$Sessies = new Sessies();
		$Sessies = $Sessies->lijst($sql);
		$t = new TemplateEngine("SessiesLijst.html");
		$t->assign("Sessies",$Sessies);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$Sessies = new Sessies();
		$Sessies = $Sessies->lijst($sql);
		$t = new TemplateEngine("browseSessiesLijst.html");
		$t->assign("Sessies",$Sessies);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->Sessies->getMessages());
		$t = new TemplateEngine("SessiesDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Sessies);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseSessiesDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Sessies);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->Sessies->validate($post);
		$m = $this->Sessies->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->Sessies->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->Sessies->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->Sessies->remove();
		return $this->quit();
	}
}

?>