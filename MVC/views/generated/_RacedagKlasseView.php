<?php

class _RacedagKlasseView extends View {	
	/**
	 *
	 * @var type RacedagKlasse	 
	 */
	
	protected $RacedagKlasse;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->RacedagKlasse = new RacedagKlasse($uid);
	}
	
	public function getInstance() {
		return $this->RacedagKlasse;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getRacedagKlasse () {
		return $this->RacedagKlasse;
	}
	
	public function lijst($sql = null)	{	
		$RacedagKlasse = new RacedagKlasse();
		$RacedagKlasse = $RacedagKlasse->lijst($sql);
		$t = new TemplateEngine("RacedagKlasseLijst.html");
		$t->assign("RacedagKlasse",$RacedagKlasse);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$RacedagKlasse = new RacedagKlasse();
		$RacedagKlasse = $RacedagKlasse->lijst($sql);
		$t = new TemplateEngine("browseRacedagKlasseLijst.html");
		$t->assign("RacedagKlasse",$RacedagKlasse);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->RacedagKlasse->getMessages());
		$t = new TemplateEngine("RacedagKlasseDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->RacedagKlasse);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseRacedagKlasseDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->RacedagKlasse);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->RacedagKlasse->validate($post);
		$m = $this->RacedagKlasse->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->RacedagKlasse->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->RacedagKlasse->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->RacedagKlasse->remove();
		return $this->quit();
	}
}

?>