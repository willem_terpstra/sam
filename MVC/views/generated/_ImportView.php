<?php

class _ImportView extends View {	
	/**
	 *
	 * @var type Import	 
	 */
	
	protected $Import;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->Import = new Import($uid);
	}
	
	public function getInstance() {
		return $this->Import;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getImport () {
		return $this->Import;
	}
	
	public function lijst($sql = null)	{	
		$Import = new Import();
		$Import = $Import->lijst($sql);
		$t = new TemplateEngine("ImportLijst.html");
		$t->assign("Import",$Import);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$Import = new Import();
		$Import = $Import->lijst($sql);
		$t = new TemplateEngine("browseImportLijst.html");
		$t->assign("Import",$Import);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->Import->getMessages());
		$t = new TemplateEngine("ImportDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Import);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseImportDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Import);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->Import->validate($post);
		$m = $this->Import->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->Import->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->Import->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->Import->remove();
		return $this->quit();
	}
}

?>