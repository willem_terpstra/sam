<?php

class _ResultaatView extends View {	
	/**
	 *
	 * @var type Resultaat	 
	 */
	
	protected $Resultaat;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->Resultaat = new Resultaat($uid);
	}
	
	public function getInstance() {
		return $this->Resultaat;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getResultaat () {
		return $this->Resultaat;
	}
	
	public function lijst($sql = null)	{	
		$Resultaat = new Resultaat();
		$Resultaat = $Resultaat->lijst($sql);
		$t = new TemplateEngine("ResultaatLijst.html");
		$t->assign("Resultaat",$Resultaat);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$Resultaat = new Resultaat();
		$Resultaat = $Resultaat->lijst($sql);
		$t = new TemplateEngine("browseResultaatLijst.html");
		$t->assign("Resultaat",$Resultaat);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->Resultaat->getMessages());
		$t = new TemplateEngine("ResultaatDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Resultaat);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseResultaatDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Resultaat);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->Resultaat->validate($post);
		$m = $this->Resultaat->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->Resultaat->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->Resultaat->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->Resultaat->remove();
		return $this->quit();
	}
}

?>