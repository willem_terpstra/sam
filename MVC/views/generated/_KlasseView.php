<?php

class _KlasseView extends View {	
	/**
	 *
	 * @var type Klasse	 
	 */
	
	protected $Klasse;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->Klasse = new Klasse($uid);
	}
	
	public function getInstance() {
		return $this->Klasse;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getKlasse () {
		return $this->Klasse;
	}
	
	public function lijst($sql = null)	{	
		$Klasse = new Klasse();
		$Klasse = $Klasse->lijst($sql);
		$t = new TemplateEngine("KlasseLijst.html");
		$t->assign("Klasse",$Klasse);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$Klasse = new Klasse();
		$Klasse = $Klasse->lijst($sql);
		$t = new TemplateEngine("browseKlasseLijst.html");
		$t->assign("Klasse",$Klasse);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->Klasse->getMessages());
		$t = new TemplateEngine("KlasseDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Klasse);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseKlasseDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Klasse);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->Klasse->validate($post);
		$m = $this->Klasse->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->Klasse->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->Klasse->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->Klasse->remove();
		return $this->quit();
	}
}

?>