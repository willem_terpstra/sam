<?php

class _RacedagView extends View {	
	/**
	 *
	 * @var type Racedag	 
	 */
	
	protected $Racedag;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->Racedag = new Racedag($uid);
	}
	
	public function getInstance() {
		return $this->Racedag;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getRacedag () {
		return $this->Racedag;
	}
	
	public function lijst($sql = null)	{	
		$Racedag = new Racedag();
		$Racedag = $Racedag->lijst($sql);
		$t = new TemplateEngine("RacedagLijst.html");
		$t->assign("Racedag",$Racedag);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$Racedag = new Racedag();
		$Racedag = $Racedag->lijst($sql);
		$t = new TemplateEngine("browseRacedagLijst.html");
		$t->assign("Racedag",$Racedag);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->Racedag->getMessages());
		$t = new TemplateEngine("RacedagDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Racedag);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseRacedagDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->Racedag);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->Racedag->validate($post);
		$m = $this->Racedag->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->Racedag->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->Racedag->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->Racedag->remove();
		return $this->quit();
	}
}

?>