<?php

class _VwRondesResultaatView extends View {	
	/**
	 *
	 * @var type VwRondesResultaat	 
	 */
	
	protected $VwRondesResultaat;
	private	$redirectAfter = NULL;

	function __construct($uid = NULL)	{	
		$this->VwRondesResultaat = new VwRondesResultaat($uid);
	}
	
	public function getInstance() {
		return $this->VwRondesResultaat;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function setMessages($messages) {
		if(is_array($messages)) {
			$this->messages = array_merge($this->messages, $messages);
		} else {
			$this->messages[] = $messages;
		}
	}	
	
	public function getVwRondesResultaat () {
		return $this->VwRondesResultaat;
	}
	
	public function lijst($sql = null)	{	
		$VwRondesResultaat = new VwRondesResultaat();
		$VwRondesResultaat = $VwRondesResultaat->lijst($sql);
		$t = new TemplateEngine("VwRondesResultaatLijst.html");
		$t->assign("VwRondesResultaat",$VwRondesResultaat);
		return $t->execTemplate();		
	}

	public function browseLijst($sql = null)
	{	$VwRondesResultaat = new VwRondesResultaat();
		$VwRondesResultaat = $VwRondesResultaat->lijst($sql);
		$t = new TemplateEngine("browseVwRondesResultaatLijst.html");
		$t->assign("VwRondesResultaat",$VwRondesResultaat);
		return $t->execTemplate();		
	}
	
	public function detail()	{
		$messages = array_merge($this->getMessages(), $this->VwRondesResultaat->getMessages());
		$t = new TemplateEngine("VwRondesResultaatDetail.html");
		$t->assign('messages', $messages);
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->VwRondesResultaat);
		
		return $t->execTemplate();
	}
	
	public function browseDetail()	{
		$t = new TemplateEngine("browseVwRondesResultaatDetail.html");
		$t->assign ("subDir", $this->subDir);
		$t->assign("record",$this->VwRondesResultaat);
		
		return $t->execTemplate();
	}
	
	public function validate() {
		$post = Post::$postVars;
		$r = $this->VwRondesResultaat->validate($post);
		$m = $this->VwRondesResultaat->getMessages();
		if(empty($m)) {
			return true;
		}
		return false;
	}
	
	public function save()
	{	
		$post = Post::$postVars;
		
		
		try {
			$this->VwRondesResultaat->saveOrUpdate();
		} catch (dbTableException $e) {
			$this->VwRondesResultaat->setMessage($e->getMessage());
			return $this->detail();	
		}
		return $this->quit();		

	}
	
	public function quit() {
		if($this->redirectAfter == NULL ) {
			return $this->lijst();
		}
		header ("location: {$this->redirectAfter}");		
	}
	
	protected function setRedirectAfter($url) {
		$this->redirectAfter = $url;
	}
	
	public function remove() {	
		$this->VwRondesResultaat->remove();
		return $this->quit();
	}
}

?>