<?php

if (!$config = simplexml_load_file('sys/config/config.xml')) {
    trigger_error('Error reading XML file', E_USER_ERROR);
}

require_once "../autoload.php";

mysql_connect("localhost", $config->mysql->user, $config->mysql->pwd);
mysql_select_db($config->mysql->db);

class MVC extends Strict {

    const TEMPLATE_DIR = "/MVC/sys/template/";
    const GENERATED_DIR = "entity/generated/";
    const CLASS_DIR = "entity/";
    const LIB_DIR = "sys/classes/lib/";
    const GENERATED_PREFIX = "_";
    const CONTROLLER_DIR = "controllers/";
    const VIEW_DIR = "views/";
    const GENERATED_VIEW_DIR = "views/generated/";
    const HTML_DIR = "html/";

    private $entityGenerate = true;
    private $controllerGenerate = true;
    private $viewGenerate = true;
    private $htmlGenerate = true;
    private $frontendGenerate = true;
    private $sysTemplatePath = '';

    public function main() {
        global $config;
        mysql_select_db($config->mysql->db);

        $this->sysTemplatePath = $_SERVER['DOCUMENT_ROOT'] . self::TEMPLATE_DIR;

        $sql = "SHOW TABLES";
        $rs = mysql_query($sql);

        $metaTableSettings = new MetaTableSettings();

        while ($row = mysql_fetch_assoc($rs)) {
            $privates = '';
            $fillers = '';
            $getters = '';
            $setters = '';
            $viewSetters = '';
            $fkMethods = '';
            $modifiedMap = '';
            $soapDeclarations = '';
            $soapOut = '';
            $lijstRecords = '';
            $browseLijstRecords = '';
            $detailRecords = '';
            $browseDetailRecords = "";
            $metaTableSettingsAvailable = false;
            $this->entityGenerate = true;
            $this->controllerGenerate = true;
            $this->viewGenerate = true;
            $this->htmlGenerate = true;
            $this->frontendGenerate = true;

            $table_name = $row['Tables_in_' . $config->mysql->db];
            $sql = "SHOW FIELDS FROM `$table_name`";

            if (strstr($table_name, 'status')) {
                $this->controllerGenerate = false;
                $this->viewGenerate = false;
                $this->htmlGenerate = false;
            }

            $metaTableSettingsAvailable = $metaTableSettings->loadByProperty('target_table', $table_name);
            if ($metaTableSettingsAvailable) {
                echo 'meta_table_settings found for ' . $table_name . '<br/>';
                $this->entityGenerate = ($metaTableSettings->getCreateEntity() == '1');
                $this->viewGenerate = ($metaTableSettings->getCreateView() == '1' && $this->entityGenerate);
                $this->controllerGenerate = ($metaTableSettings->getCreateController() == '1' && $this->viewGenerate);
                $this->htmlGenerate = ($metaTableSettings->getCreateHtml() == '1' && $this->viewGenerate);
                $this->frontendGenerate = ($metaTableSettings->getCreateFrontend() == '1' && $this->htmlGenerate);
            } else {
                echo 'no meta_table_settings for ' . $table_name . '<br/>';
                continue;
            }

            $rsFields = mysql_query($sql);
            $className = $this->camelCase($table_name);
            if ($this->entityGenerate) {
                $generatedFilePath = self::GENERATED_DIR . self::GENERATED_PREFIX . $className . ".php";
                $content = $this->superEntityGenerate($table_name, $className, $rsFields);
                $this->saveFile($generatedFilePath, $content);

                // Data model class, do not overwrite
                $filePath = self::CLASS_DIR . $className . ".php";
                if (!file_exists($filePath)) {
                    $content = $this->subEntityGenerate($className, $rsFields);
                    $this->saveFile($filePath, $content);
                }
            } else {
                echo "class $className NOT generated (see meta_table_settings)<br/>";
                continue;
            }



            if ($this->viewGenerate) {
                $filePath = $_SERVER['DOCUMENT_ROOT'] . "/MVC/" . self::GENERATED_VIEW_DIR . self::GENERATED_PREFIX . $className . "View.php";
                $content = $this->superViewGenerate($className, $viewSetters);
                $this->saveFile($filePath, $content);

                // subview
                $filePath = $_SERVER['DOCUMENT_ROOT'] . "/MVC/" . self::VIEW_DIR . $className . "View.php";
                if (!file_exists($filePath)) {
                    $content = $this->subViewGenerate($className);
                    $this->saveFile($filePath, $content);
                }
            } else {
                echo "view $className NOT generated (see meta_table_settings)<br/>";
                continue;
            }

            if ($this->controllerGenerate) {
                $filePath = $_SERVER['DOCUMENT_ROOT'] . "/MVC/" . self::CONTROLLER_DIR . $className . "Controller.php";
                if (!file_exists($filePath)) {
                    $content = $this->controllerGenerate($className);
                    $this->saveFile($filePath, $content);
                }
            } else {
                echo "controller $className NOT generated (see meta_table_settings)<br/>";
                continue;
            }

            // generate html late, otherwise you cannot use classes in meta html
            if ($this->htmlGenerate) {
                $rsFields = null;
                $rsFields = mysql_query($sql);
                while ($row = mysql_fetch_assoc($rsFields)) {
                    $fieldName = $row['Field'];
                    $fieldType = $row['Type'];
                    $camelCase = $this->camelCase($fieldName);

                    $t = new TemplateEngine($this->sysTemplatePath . "lijstRecords.html");
                    $t->assign('className', $className);
                    $t->assign('funcName', $camelCase);
                    $t->assign('mode', 'edit');
                    $lijstRecords .= $t->execTemplate();

                    $t = new TemplateEngine($this->sysTemplatePath . "lijstRecords.html");
                    $t->assign('className', $className);
                    $t->assign('funcName', $camelCase);
                    $t->assign('mode', 'browse');
                    $browseLijstRecords .= $t->execTemplate();



                    $t = new TemplateEngine($this->sysTemplatePath . "detailRecords.php");
                    $t->assign('className', $className);
                    $t->assign('funcName', $camelCase);
                    $t->assign('varName', $fieldName);
                    $t->assign('fieldType', $fieldType);
                    $t->assign('includePath', self::CLASS_DIR);
                    $detailRecords .= $t->execTemplate();

                    $t = new TemplateEngine($this->sysTemplatePath . "browseDetailRecords.php");
                    $t->assign('className', $className);
                    $t->assign('funcName', $camelCase);
                    $t->assign('varName', $fieldName);
                    $t->assign('fieldType', $fieldType);
                    $t->assign('includePath', self::CLASS_DIR);
                    $browseDetailRecords .= $t->execTemplate();
                }

                // Backend HTML
                $filePath = $_SERVER['DOCUMENT_ROOT'] . "/MVC/" . self::HTML_DIR . $className . "Lijst.html";
                if (!file_exists($filePath)) {
                    $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . self::TEMPLATE_DIR . "lijstTemplate.html");
                    $t->assign('className', $className);
                    $t->assign('lijstRecords', $lijstRecords);

                    $content = $t->execTemplate();
                    $this->saveFile($filePath, $content);
                }

                // Frontend HTML
                if ($this->frontendGenerate) {
                    $filePath = $_SERVER['DOCUMENT_ROOT'] . "/MVC/" . self::HTML_DIR . 'browse' . $className . "Lijst.html";
                    if (!file_exists($filePath)) {
                        $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . self::TEMPLATE_DIR . "browseLijstTemplate.html");
                        $t->assign('className', $className);
                        $t->assign('lijstRecords', $browseLijstRecords);

                        $content = $t->execTemplate();
                        $this->saveFile($filePath, $content);
                    }
                } else {
                    echo 'frontend generate switched off for ' . $table_name . ' (default for status tables)<br/>';
                }


                // Detail HTML
                $filePath = $_SERVER['DOCUMENT_ROOT'] . "/MVC/" . self::HTML_DIR . $className . "Detail.html";
                if (!file_exists($filePath)) {
                    $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . self::TEMPLATE_DIR . "detailTemplate.php");
                    $t->assign('className', $className);
                    $t->assign('detailRecords', $detailRecords);
                    $content = $t->execTemplate();

                    $this->saveFile($filePath, $content);
                }

                // Browse detail HTML
                $filePath = $_SERVER['DOCUMENT_ROOT'] . "/MVC/" . self::HTML_DIR . 'browse' . $className . "Detail.html";
                if (!file_exists($filePath)) {
                    $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . self::TEMPLATE_DIR . "browseDetailTemplate.php");
                    $t->assign('className', $className);
                    $t->assign('browseDetailRecords', $browseDetailRecords);
                    $content = $t->execTemplate();
                    $this->saveFile($filePath, $content);
                }
            } else {
                echo "HTML not generated for table: $table_name<br/>";
            }
        }
    }

    private function superEntityGenerate($tableName, $className, $rsFields) {
        // TODO: check if uid exists
        $privates = '';
        $modifiedMap = '';
        $soapDeclarations = '';
        $soapOut = '';
        $fillers = '';
        $fkMethods = '';
        $viewSetters = '';
        while ($row = mysql_fetch_assoc($rsFields)) {
            $fieldName = $row['Field'];
            $camelCase = $this->camelCase($fieldName);

            $t = new TemplateEngine($this->sysTemplatePath . "private.php");
            $t->assign('varName', $fieldName);
            $privates .= $t->execTemplate();

            $t = new TemplateEngine($this->sysTemplatePath . "modifiedMap.php");
            $t->assign('varName', $fieldName);
            $modifiedMap .= $t->execTemplate();

            $t = new TemplateEngine($this->sysTemplatePath . "soapDeclarations.php");
            $t->assign('varName', $fieldName);
            $soapDeclarations .= $t->execTemplate();

            $t = new TemplateEngine($this->sysTemplatePath . "soapOut.php");
            $t->assign('varName', $fieldName);
            $soapOut .= $t->execTemplate();

            $t = new TemplateEngine($this->sysTemplatePath . "fillers.php");
            $t->assign('varName', $fieldName);
            $fillers .= $t->execTemplate();

            $t = new TemplateEngine($this->sysTemplatePath . "fkMethods.php");
            $t->assign('funcName', $camelCase);
            $t->assign('varName', $fieldName);
            $fkMethods .= $t->execTemplate();

            $t = new TemplateEngine($this->sysTemplatePath . "viewSetters.php");
            $t->assign('className', $className);
            $t->assign('funcName', $camelCase);
            $t->assign('varName', $fieldName);
            $viewSetters .= $t->execTemplate();
        }

        // Data Model base class
        $t = new TemplateEngine($this->sysTemplatePath . "generatedClassTemplate.php");
        $t->assign('table_name', $tableName);
        $t->assign('className', self::GENERATED_PREFIX . $className);
        $t->assign('modifiedMap', $modifiedMap);
        $t->assign('soapDeclarations', $soapDeclarations);
        $t->assign('soapOut', $soapOut);
        $t->assign('privates', $privates);
        $t->assign('fillers', $fillers);
        $t->assign('fkMethods', $fkMethods);

        return $t->execTemplate();
    }

    private function subEntityGenerate($className, $rsFields) {
        $t = new TemplateEngine($this->sysTemplatePath . "classTemplate.php");
        $t->assign('extendClass', self::GENERATED_PREFIX . $className);
        $t->assign('className', $className);

        return $t->execTemplate();
    }

    private function controllerGenerate($className) {
        $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . self::TEMPLATE_DIR . "controllerTemplate.php");
        $t->assign('className', $className);
        return $t->execTemplate();
    }

    private function superViewGenerate($className, $viewSetters) {
        $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . self::TEMPLATE_DIR . "generatedViewTemplate.php");
        $t->assign('className', $className);
        $t->assign('setters', $viewSetters);
        return $t->execTemplate();
    }

    private function subViewGenerate($className) {
        $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . self::TEMPLATE_DIR . "viewTemplate.php");
        $t->assign('className', $className);
        return $t->execTemplate();
    }

    private function saveFile($filePath, $content) {
        $handle = fopen($filePath, "w");
        fwrite($handle, $content);
        fclose($handle);
        echo "file generated: $filePath<br/>";
    }

    private function camelCase($fieldName) {
        $arr = explode("_", $fieldName);
        $result = "";
        foreach ($arr as $part) {
            $result .= ucfirst($part);
        }
        $result = ucfirst($result);
        return $result;
    }

}

$c = new MVC();
$c->main();
?>