<?php

class SessieNaam extends _SessieNaam implements DropdownAble {

    public function getDropdownDefaultValue() {
        return NULL;
    }

    public function getDropdownNullOption() {
        return NULL;
    }

    public function getIntValueMethod() {
        return 'getUid';
    }

    public function getValue() {
        return $this->naam;
    }

    /* system functions */

    public function getClassName() {
        return "SessieNaam";
    }

    public function getUid() {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id) {
        $idName = $this->getPK();
        $this->$idName = $id;
    }
    function __toString() {
        return $this->naam;
    }

}

?>