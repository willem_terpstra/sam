<?php

class VwUitslag extends _VwUitslag {

    public function getRondeResultaatPerc(VwKlasseTotalen $vwKlasseTotaal) {
        $vwRondesResultaat = new VwRondesResultaat();
        if (!$vwKlasseTotaal->getSessiesId()) {
            return false;
        }
        $lijst = $vwRondesResultaat->lijst("SELECT * FROM vw_rondes_resultaat 
            WHERE sessies_id={$vwKlasseTotaal->getSessiesId()}
            AND resultaat_naam = '{$vwKlasseTotaal->getResultaatNaam()}'   "
        );
        $rondeResultaat = $lijst->first();
        return $rondeResultaat->getRondesPerc();
    }

    public function getRondesMancheOne($checkFail = true) {
        $vwKlasseTotalen = new VwKlasseTotalen();
        $lijst = $vwKlasseTotalen->lijst("SELECT * FROM vw_klasse_totalen 
            WHERE racedag_klasse_id={$this->racedag_klasse_id} 
                AND sessie_naam='1e manche'
                AND resultaat_naam='{$this->resultaat_naam}';
            ");

        $vwKlasseTotaal = $lijst->first();
        $perc = $this->getRondeResultaatPerc($vwKlasseTotaal);
        if ($perc !== false) {
            $perc = number_format($perc, 2);
            $allowedPerc = $vwKlasseTotaal->getRacedag()->getMinRondenPerc();
            if ($checkFail) {
                if ($allowedPerc > $perc) {
                    throw new VwUitslagException('not enough laps', VwUitslagException::NOT_ENOUGH_LAPS);
                }
            }
        } else {
            throw new VwUitslagException('no laps', VwUitslagException::NO_LAPS);
        }

        return $vwKlasseTotaal->getResultaatRondes()
                . '&nbsp;&nbsp; ('
                . ($perc === false ? 'n/a' : $perc . '%') . ')';
    }

    public function getRondesMancheTwo($checkFail = true) {
        $vwKlasseTotalen = new VwKlasseTotalen();
        $lijst = $vwKlasseTotalen->lijst("SELECT * FROM vw_klasse_totalen 
            WHERE racedag_klasse_id={$this->racedag_klasse_id} 
                AND sessie_naam='2e manche'
                AND resultaat_naam='{$this->resultaat_naam}';
            ");
        $vwKlasseTotaal = $lijst->first();
        $perc = $this->getRondeResultaatPerc($vwKlasseTotaal);
        if ($perc !== false) {
            $perc = number_format($perc, 2);
            $allowedPerc = $vwKlasseTotaal->getRacedag()->getMinRondenPerc();
            if ($checkFail) {
                if ($allowedPerc > $perc) {
                    throw new VwUitslagException('not enough laps', VwUitslagException::NOT_ENOUGH_LAPS);
                }
            }
        } else {
            throw new VwUitslagException('no laps', VwUitslagException::NO_LAPS);
        }
        return $vwKlasseTotaal->getResultaatRondes()
                . '&nbsp;&nbsp; ('
                . ($perc === false ? 'n/a' : $perc . '%') . ')';
    }

    /* system functions */

    public function getClassName() {
        return "VwUitslag";
    }

    public function getUid() {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id) {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}
