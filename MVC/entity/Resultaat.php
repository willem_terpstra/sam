<?php

class Resultaat extends _Resultaat {
    public function calculate($lapInfo, $importFileId) {
        $importFile = new ImportFile($importFileId);
        $minRonden = $importFile->getMinRonden();
        $this->execSql("DELETE FROM resultaat WHERE import_file_id = $importFileId");
        foreach ($lapInfo as $rider => $laps) {
            $lapCnt = count($laps);
            $avg = array_sum($laps) / $lapCnt;
            $diff = 0;
            foreach ($laps as $lap) {
                $diff += abs($avg - $lap);
            }
            $resultaat = new Resultaat();
            $resultaat->setNaam($rider);
            $resultaat->setMsecStraf($diff);
            $resultaat->setMsecGemiddeld($avg);
            $resultaat->setRondes($lapCnt);
            $resultaat->setQualified((int)($lapCnt >= $minRonden));
            $resultaat->setImportFileId($importFileId);
            $resultaat->save();
        }
        return 'verwerkt';
    }

    public function getStraf() {
        return self::timeFormat($this->msec_straf);
    }

    public function getGemiddeld() {
        return self::timeFormat($this->msec_gemiddeld);
    }

    public static function timeFormat($msec) {

        $sec = floor($msec / 1000);
        $msec =  substr($msec, -3);
        $min = floor($sec / 60);
        $sec -= floor($min * 60);
        if ($sec < 10) {
            $sec = "0$sec";
        }
        if ($min < 10) {
            $min = "0$min";
        }

        return "$min:$sec.$msec";
    }

    public function getClassName() {
        return "Resultaat";
    }

    public function getUid() {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id) {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}
