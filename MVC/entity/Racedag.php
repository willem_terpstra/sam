<?php

class Racedag extends _Racedag implements DropdownAble {

    public function getDatum() {
        if (empty($this->datum)) {
            $this->datum = date('Y-m-d');
        }
        $a = explode('-', $this->datum);
//        $l = mb_strlen($a[0]);
        if (mb_strlen($a[0]) == 4) {
            return $a[2] . '-' . $a[1] . '-' . $a[0];
        } else {
            return $this->datum;
        }
    }

    public function removeChildren() {
        $racedagKlasse = new RacedagKlasse();
        foreach ($racedagKlasse->lijst('SELECT * FROM racedag_klasse WHERE racedag_id =' . $this->uid) as $racedagKlasse) {
            $sessies = new Sessies();
            foreach ($sessies->lijst('SELECT * FROM sessies WHERE racedag_klasse_id =' . $racedagKlasse->getUid()) as $sessie) {
                $this->execSql('DELETE FROM import_file WHERE sessies_id = ' . $sessie->getUid());
            }
            $this->execSql('DELETE FROM sessies WHERE racedag_klasse_id =' . $racedagKlasse->getUid());
        }
        $this->execSql('DELETE FROM racedag_klasse WHERE racedag_id =' . $this->uid);
    }

    /* system functions */

    public function getClassName() {
        return "Racedag";
    }

    public function getUid() {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id) {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

    public function getDropdownDefaultValue() {
        
    }

    public function getDropdownNullOption() {
        
    }

    public function getIntValueMethod() {
        
    }

    public function getValue() {
        return $this->naam . ' ' . $this->datum;
    }

    function __toString() {
        return $this->getValue();
    }

}
