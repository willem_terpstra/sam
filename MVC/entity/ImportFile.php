<?php

class ImportFile extends _ImportFile {

    public function getMinRonden() {
        $minRondenPerc = $this->getSessies()->getRacedagKlasse()->getRacedag()->getMinRondenPerc();
        $minRondenPerc /= 100;
        $requiredLaps = sqlGet("SELECT MAX(rondes) * $minRondenPerc AS max_rondes FROM resultaat WHERE import_file_id=" . $this->uid);
        return $requiredLaps;
    }

    public function read() {
        $output = array();
        $handle = fopen($_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $this->name, 'r');
        if ($handle) {
            while (($buffer = fgetcsv($handle, 4096)) !== false) {

                $time = explode('.', $buffer[1]);
                $left = explode(':', $time[0]);
                if (count($left) < 2) {
                    if (!is_numeric($left[0]))
                        continue;
                    else
                        array_unshift($left, 0);
                }
                if (!array_key_exists($buffer[0], $output)) {
                    $output[$buffer[0]] = array();
                }
                $msec = 1000 * ($left[0] * 60 + $left[1]) + $time[1];
                $output[$buffer[0]][] = (string) $msec;
            }
//            var_export($output);
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        }
        return $output;
    }

    public function getKlasseNaam() {
        $klasse = new Klasse($this->klasse_id);
        return $klasse->getNaam();
    }

    public function getSessieNaam($uid = NULL) {
        $sessie = new SessieNaam($this->sessie_naam_id);
        return $sessie->getNaam();
    }

    public function getResultaatAantal() {
        $resultaat = new Resultaat();
        $resultaat = $resultaat->lijst('SELECT * FROM resultaat WHERE import_file_id=' . $this->uid);
        return count($resultaat);
    }

    /* system functions */

    public function getClassName() {
        return "ImportFile";
    }

    public function getUid() {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id) {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}
