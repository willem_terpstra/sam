<?php

class RacedagKlasse extends _RacedagKlasse implements DropdownAble {

    public function getKlasse($uid = NULL) {
        if (!$this->getKlasseId())
            throw new RacedagKlasseException('Klasse not found');
        if (empty($uid)) {
            return new Klasse($this->getKlasseId());
        }
        $Klasse = new Klasse($uid);
        return $Klasse->getValue();
    }

    /* system functions */

    public function getClassName() {
        return "RacedagKlasse";
    }

    public function getUid() {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id) {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

    public function getDropdownDefaultValue() {
        
    }

    public function getDropdownNullOption() {
        
    }

    public function getIntValueMethod() {
        return 'getUid';
    }

    public function getValue() {
//        return $this->getKlasse();
        $klasse = $this->getKlasse();
        $racedag = $this->getRacedag();
        return
                $klasse->getNaam() . ' '
                . $racedag->getNaam() . ' '
                . $racedag->getDatum();
    }

    public function __toString() {
        return $this->getValue();
//        $klasse = $this->getKlasse();
//        $racedag = $this->getRacedag();
//        return
//                $klasse->getNaam() .' '
//                . $racedag->getNaam() . ' '
//                . $racedag->getDatum();
    }

}

