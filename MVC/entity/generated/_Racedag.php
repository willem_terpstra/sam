<?php 
class _Racedag extends dbTable	{
	/* db table */
	protected	$tableName = 'racedag';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'datum' => false,
		'naam' => false,
		'min_ronden_perc' => false,	);

	/* properties */
	protected $uid;
	protected $datum;
	protected $naam;
	protected $min_ronden_perc;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'datum' => array('name' => 'datum', 'type' => 'xsd:string'),
		'naam' => array('name' => 'naam', 'type' => 'xsd:string'),
		'min_ronden_perc' => array('name' => 'min_ronden_perc', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_Racedag";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"datum" => $this->datum, 
		"naam" => $this->naam, 
		"min_ronden_perc" => $this->min_ronden_perc, 
		);	  
	}

	
	/* FK methods */
	
}
?>