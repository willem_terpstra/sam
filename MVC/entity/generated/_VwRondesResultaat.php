<?php 
class _VwRondesResultaat extends dbTable	{
	/* db table */
	protected	$tableName = 'vw_rondes_resultaat';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'resultaat_naam' => false,
		'sessies_id' => false,
		'sessie_naam' => false,
		'rondes' => false,
		'max_rondes' => false,
		'rondes_perc' => false,	);

	/* properties */
	protected $uid;
	protected $resultaat_naam;
	protected $sessies_id;
	protected $sessie_naam;
	protected $rondes;
	protected $max_rondes;
	protected $rondes_perc;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'resultaat_naam' => array('name' => 'resultaat_naam', 'type' => 'xsd:string'),
		'sessies' => array('name' => 'sessies', 'type' => 'xsd:string'),
		'sessie_naam' => array('name' => 'sessie_naam', 'type' => 'xsd:string'),
		'rondes' => array('name' => 'rondes', 'type' => 'xsd:string'),
		'max_rondes' => array('name' => 'max_rondes', 'type' => 'xsd:string'),
		'rondes_perc' => array('name' => 'rondes_perc', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
		$this->is_view = true;
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_VwRondesResultaat";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"resultaat_naam" => $this->resultaat_naam, 
		"sessies" => $this->getSessies($this->sessies_id),
		"sessie_naam" => $this->sessie_naam, 
		"rondes" => $this->rondes, 
		"max_rondes" => $this->max_rondes, 
		"rondes_perc" => $this->rondes_perc, 
		);	  
	}

	
	/* FK methods */
			
		public function getSessiesList ($sql = null)	{
			$Sessies = new Sessies();
			$list = array();
			$list = $Sessies->lijst($sql);
			return $list;
		}
		
		public function getSessies ($uid = NULL)	{
			if(empty($uid)) {
				return new Sessies($this->getSessiesId());
			}
			$Sessies = new Sessies($uid);
			return $Sessies->getValue();
		}

}
?>