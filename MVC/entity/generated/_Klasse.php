<?php 
class _Klasse extends dbTable	{
	/* db table */
	protected	$tableName = 'klasse';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'naam' => false,	);

	/* properties */
	protected $uid;
	protected $naam;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'naam' => array('name' => 'naam', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_Klasse";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"naam" => $this->naam, 
		);	  
	}

	
	/* FK methods */
	
}
?>