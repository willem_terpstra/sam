<?php 
class _RacedagKlasse extends dbTable	{
	/* db table */
	protected	$tableName = 'racedag_klasse';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'racedag_id' => false,
		'klasse_id' => false,	);

	/* properties */
	protected $uid;
	protected $racedag_id;
	protected $klasse_id;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'racedag' => array('name' => 'racedag', 'type' => 'xsd:string'),
		'klasse' => array('name' => 'klasse', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_RacedagKlasse";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"racedag" => $this->getRacedag($this->racedag_id),
		"klasse" => $this->getKlasse($this->klasse_id),
		);	  
	}

	
	/* FK methods */
			
		public function getRacedagList ($sql = null)	{
			$Racedag = new Racedag();
			$list = array();
			$list = $Racedag->lijst($sql);
			return $list;
		}
		
		public function getRacedag ($uid = NULL)	{
			if(empty($uid)) {
				return new Racedag($this->getRacedagId());
			}
			$Racedag = new Racedag($uid);
			return $Racedag->getValue();
		}
		
		public function getKlasseList ($sql = null)	{
			$Klasse = new Klasse();
			$list = array();
			$list = $Klasse->lijst($sql);
			return $list;
		}
		
		public function getKlasse ($uid = NULL)	{
			if(empty($uid)) {
				return new Klasse($this->getKlasseId());
			}
			$Klasse = new Klasse($uid);
			return $Klasse->getValue();
		}

}
?>