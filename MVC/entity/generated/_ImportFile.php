<?php 
class _ImportFile extends dbTable	{
	/* db table */
	protected	$tableName = 'import_file';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'sessies_id' => false,
		'status' => false,
		'name' => false,	);

	/* properties */
	protected $uid;
	protected $sessies_id;
	protected $status;
	protected $name;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'sessies' => array('name' => 'sessies', 'type' => 'xsd:string'),
		'status' => array('name' => 'status', 'type' => 'xsd:string'),
		'name' => array('name' => 'name', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_ImportFile";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"sessies" => $this->getSessies($this->sessies_id),
		"status" => $this->status, 
		"name" => $this->name, 
		);	  
	}

	
	/* FK methods */
			
		public function getSessiesList ($sql = null)	{
			$Sessies = new Sessies();
			$list = array();
			$list = $Sessies->lijst($sql);
			return $list;
		}
		
		public function getSessies ($uid = NULL)	{
			if(empty($uid)) {
				return new Sessies($this->getSessiesId());
			}
			$Sessies = new Sessies($uid);
			return $Sessies->getValue();
		}

}
?>