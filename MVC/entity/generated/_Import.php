<?php 
class _Import extends dbTable	{
	/* db table */
	protected	$tableName = 'import';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'naam' => false,
		'tijd' => false,	);

	/* properties */
	protected $uid;
	protected $naam;
	protected $tijd;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'naam' => array('name' => 'naam', 'type' => 'xsd:string'),
		'tijd' => array('name' => 'tijd', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_Import";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"naam" => $this->naam, 
		"tijd" => $this->tijd, 
		);	  
	}

	
	/* FK methods */
	
}
?>