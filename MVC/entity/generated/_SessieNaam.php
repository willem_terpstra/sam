<?php 
class _SessieNaam extends dbTable	{
	/* db table */
	protected	$tableName = 'sessie_naam';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'naam' => false,	);

	/* properties */
	protected $uid;
	protected $naam;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'naam' => array('name' => 'naam', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_SessieNaam";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"naam" => $this->naam, 
		);	  
	}

	
	/* FK methods */
	
}
?>