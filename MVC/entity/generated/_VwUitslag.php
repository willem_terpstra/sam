<?php 
class _VwUitslag extends dbTable	{
	/* db table */
	protected	$tableName = 'vw_uitslag';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'racedag_klasse_id' => false,
		'resultaat_naam' => false,
		'straf' => false,
		'klasse_naam' => false,
		'rondes' => false,
		'racedag_naam' => false,
		'racedag_datum' => false,	);

	/* properties */
	protected $uid;
	protected $racedag_klasse_id;
	protected $resultaat_naam;
	protected $straf;
	protected $klasse_naam;
	protected $rondes;
	protected $racedag_naam;
	protected $racedag_datum;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'racedag_klasse' => array('name' => 'racedag_klasse', 'type' => 'xsd:string'),
		'resultaat_naam' => array('name' => 'resultaat_naam', 'type' => 'xsd:string'),
		'straf' => array('name' => 'straf', 'type' => 'xsd:string'),
		'klasse_naam' => array('name' => 'klasse_naam', 'type' => 'xsd:string'),
		'rondes' => array('name' => 'rondes', 'type' => 'xsd:string'),
		'racedag_naam' => array('name' => 'racedag_naam', 'type' => 'xsd:string'),
		'racedag_datum' => array('name' => 'racedag_datum', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
		$this->is_view = true;
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_VwUitslag";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"racedag_klasse" => $this->getRacedagKlasse($this->racedag_klasse_id),
		"resultaat_naam" => $this->resultaat_naam, 
		"straf" => $this->straf, 
		"klasse_naam" => $this->klasse_naam, 
		"rondes" => $this->rondes, 
		"racedag_naam" => $this->racedag_naam, 
		"racedag_datum" => $this->racedag_datum, 
		);	  
	}

	
	/* FK methods */
			
		public function getRacedagKlasseList ($sql = null)	{
			$RacedagKlasse = new RacedagKlasse();
			$list = array();
			$list = $RacedagKlasse->lijst($sql);
			return $list;
		}
		
		public function getRacedagKlasse ($uid = NULL)	{
			if(empty($uid)) {
				return new RacedagKlasse($this->getRacedagKlasseId());
			}
			$RacedagKlasse = new RacedagKlasse($uid);
			return $RacedagKlasse->getValue();
		}

}
?>