<?php 
class _VwKlasseTotalen extends dbTable	{
	/* db table */
	protected	$tableName = 'vw_klasse_totalen';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'racedag_id' => false,
		'klasse_id' => false,
		'racedag_klasse_id' => false,
		'sessies_id' => false,
		'racedag_naam' => false,
		'racedag_datum' => false,
		'klasse_naam' => false,
		'sessie_naam' => false,
		'resultaat_rondes' => false,
		'msec_gemiddeld' => false,
		'msec_straf' => false,
		'resultaat_naam' => false,
		'sessie_naam_id' => false,	);

	/* properties */
	protected $uid;
	protected $racedag_id;
	protected $klasse_id;
	protected $racedag_klasse_id;
	protected $sessies_id;
	protected $racedag_naam;
	protected $racedag_datum;
	protected $klasse_naam;
	protected $sessie_naam;
	protected $resultaat_rondes;
	protected $msec_gemiddeld;
	protected $msec_straf;
	protected $resultaat_naam;
	protected $sessie_naam_id;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'racedag' => array('name' => 'racedag', 'type' => 'xsd:string'),
		'klasse' => array('name' => 'klasse', 'type' => 'xsd:string'),
		'racedag_klasse' => array('name' => 'racedag_klasse', 'type' => 'xsd:string'),
		'sessies' => array('name' => 'sessies', 'type' => 'xsd:string'),
		'racedag_naam' => array('name' => 'racedag_naam', 'type' => 'xsd:string'),
		'racedag_datum' => array('name' => 'racedag_datum', 'type' => 'xsd:string'),
		'klasse_naam' => array('name' => 'klasse_naam', 'type' => 'xsd:string'),
		'sessie_naam' => array('name' => 'sessie_naam', 'type' => 'xsd:string'),
		'resultaat_rondes' => array('name' => 'resultaat_rondes', 'type' => 'xsd:string'),
		'msec_gemiddeld' => array('name' => 'msec_gemiddeld', 'type' => 'xsd:string'),
		'msec_straf' => array('name' => 'msec_straf', 'type' => 'xsd:string'),
		'resultaat_naam' => array('name' => 'resultaat_naam', 'type' => 'xsd:string'),
		'sessie_naam' => array('name' => 'sessie_naam', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
		$this->is_view = true;
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_VwKlasseTotalen";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"racedag" => $this->getRacedag($this->racedag_id),
		"klasse" => $this->getKlasse($this->klasse_id),
		"racedag_klasse" => $this->getRacedagKlasse($this->racedag_klasse_id),
		"sessies" => $this->getSessies($this->sessies_id),
		"racedag_naam" => $this->racedag_naam, 
		"racedag_datum" => $this->racedag_datum, 
		"klasse_naam" => $this->klasse_naam, 
		"sessie_naam" => $this->sessie_naam, 
		"resultaat_rondes" => $this->resultaat_rondes, 
		"msec_gemiddeld" => $this->msec_gemiddeld, 
		"msec_straf" => $this->msec_straf, 
		"resultaat_naam" => $this->resultaat_naam, 
		"sessie_naam" => $this->getSessieNaam($this->sessie_naam_id),
		);	  
	}

	
	/* FK methods */
			
		public function getRacedagList ($sql = null)	{
			$Racedag = new Racedag();
			$list = array();
			$list = $Racedag->lijst($sql);
			return $list;
		}
		
		public function getRacedag ($uid = NULL)	{
			if(empty($uid)) {
				return new Racedag($this->getRacedagId());
			}
			$Racedag = new Racedag($uid);
			return $Racedag->getValue();
		}
		
		public function getKlasseList ($sql = null)	{
			$Klasse = new Klasse();
			$list = array();
			$list = $Klasse->lijst($sql);
			return $list;
		}
		
		public function getKlasse ($uid = NULL)	{
			if(empty($uid)) {
				return new Klasse($this->getKlasseId());
			}
			$Klasse = new Klasse($uid);
			return $Klasse->getValue();
		}
		
		public function getRacedagKlasseList ($sql = null)	{
			$RacedagKlasse = new RacedagKlasse();
			$list = array();
			$list = $RacedagKlasse->lijst($sql);
			return $list;
		}
		
		public function getRacedagKlasse ($uid = NULL)	{
			if(empty($uid)) {
				return new RacedagKlasse($this->getRacedagKlasseId());
			}
			$RacedagKlasse = new RacedagKlasse($uid);
			return $RacedagKlasse->getValue();
		}
		
		public function getSessiesList ($sql = null)	{
			$Sessies = new Sessies();
			$list = array();
			$list = $Sessies->lijst($sql);
			return $list;
		}
		
		public function getSessies ($uid = NULL)	{
			if(empty($uid)) {
				return new Sessies($this->getSessiesId());
			}
			$Sessies = new Sessies($uid);
			return $Sessies->getValue();
		}
		
		public function getSessieNaamList ($sql = null)	{
			$SessieNaam = new SessieNaam();
			$list = array();
			$list = $SessieNaam->lijst($sql);
			return $list;
		}
		
		public function getSessieNaam ($uid = NULL)	{
			if(empty($uid)) {
				return new SessieNaam($this->getSessieNaamId());
			}
			$SessieNaam = new SessieNaam($uid);
			return $SessieNaam->getValue();
		}

}
?>