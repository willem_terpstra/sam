<?php 
class _Auth extends dbTable	{
	/* db table */
	protected	$tableName = 'auth';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'level' => false,
		'naam' => false,	);

	/* properties */
	protected $uid;
	protected $level;
	protected $naam;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'level' => array('name' => 'level', 'type' => 'xsd:string'),
		'naam' => array('name' => 'naam', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_Auth";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"level" => $this->level, 
		"naam" => $this->naam, 
		);	  
	}

	
	/* FK methods */
	
}
?>