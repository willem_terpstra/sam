<?php 
class _Resultaat extends dbTable	{
	/* db table */
	protected	$tableName = 'resultaat';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'plaats' => false,
		'naam' => false,
		'msec_straf' => false,
		'msec_gemiddeld' => false,
		'rondes' => false,
		'import_file_id' => false,
		'qualified' => false,	);

	/* properties */
	protected $uid;
	protected $plaats;
	protected $naam;
	protected $msec_straf;
	protected $msec_gemiddeld;
	protected $rondes;
	protected $import_file_id;
	protected $qualified;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'plaats' => array('name' => 'plaats', 'type' => 'xsd:string'),
		'naam' => array('name' => 'naam', 'type' => 'xsd:string'),
		'msec_straf' => array('name' => 'msec_straf', 'type' => 'xsd:string'),
		'msec_gemiddeld' => array('name' => 'msec_gemiddeld', 'type' => 'xsd:string'),
		'rondes' => array('name' => 'rondes', 'type' => 'xsd:string'),
		'import_file' => array('name' => 'import_file', 'type' => 'xsd:string'),
		'qualified' => array('name' => 'qualified', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_Resultaat";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"plaats" => $this->plaats, 
		"naam" => $this->naam, 
		"msec_straf" => $this->msec_straf, 
		"msec_gemiddeld" => $this->msec_gemiddeld, 
		"rondes" => $this->rondes, 
		"import_file" => $this->getImportFile($this->import_file_id),
		"qualified" => $this->qualified, 
		);	  
	}

	
	/* FK methods */
			
		public function getImportFileList ($sql = null)	{
			$ImportFile = new ImportFile();
			$list = array();
			$list = $ImportFile->lijst($sql);
			return $list;
		}
		
		public function getImportFile ($uid = NULL)	{
			if(empty($uid)) {
				return new ImportFile($this->getImportFileId());
			}
			$ImportFile = new ImportFile($uid);
			return $ImportFile->getValue();
		}

}
?>