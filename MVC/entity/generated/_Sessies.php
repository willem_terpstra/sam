<?php 
class _Sessies extends dbTable	{
	/* db table */
	protected	$tableName = 'sessies';
	
	/* modification map */
	protected  $dbModified = array (

		'uid' => false,
		'racedag_klasse_id' => false,
		'sessie_naam_id' => false,	);

	/* properties */
	protected $uid;
	protected $racedag_klasse_id;
	protected $sessie_naam_id;

	/* soap declarations */
	public static $soapDeclarations = array(
		'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
		'racedag_klasse' => array('name' => 'racedag_klasse', 'type' => 'xsd:string'),
		'sessie_naam' => array('name' => 'sessie_naam', 'type' => 'xsd:string'),
	
	);

	/* constructor */
	public function __construct ($id = null)
	{	
	
		if ( ! is_null ($id) )
		{	$this->load($id);
		}

		parent::__construct($this->getTableName());
	}
	
	public function getClassName()
	{	return "_Sessies";
	}
	
	public function getTableName()
	{	return $this->tableName;
	}
	
	public function getSoapOut()
	{
		return array(
		"uid" => $this->uid, 
		"racedag_klasse" => $this->getRacedagKlasse($this->racedag_klasse_id),
		"sessie_naam" => $this->getSessieNaam($this->sessie_naam_id),
		);	  
	}

	
	/* FK methods */
			
		public function getRacedagKlasseList ($sql = null)	{
			$RacedagKlasse = new RacedagKlasse();
			$list = array();
			$list = $RacedagKlasse->lijst($sql);
			return $list;
		}
		
		public function getRacedagKlasse ($uid = NULL)	{
			if(empty($uid)) {
				return new RacedagKlasse($this->getRacedagKlasseId());
			}
			$RacedagKlasse = new RacedagKlasse($uid);
			return $RacedagKlasse->getValue();
		}
		
		public function getSessieNaamList ($sql = null)	{
			$SessieNaam = new SessieNaam();
			$list = array();
			$list = $SessieNaam->lijst($sql);
			return $list;
		}
		
		public function getSessieNaam ($uid = NULL)	{
			if(empty($uid)) {
				return new SessieNaam($this->getSessieNaamId());
			}
			$SessieNaam = new SessieNaam($uid);
			return $SessieNaam->getValue();
		}

}
?>