<?php

class Sessies extends _Sessies implements DropdownAble {

    const TRAINING = 1;
    const MANCHE_1 = 2;
    const MANCHE_2 = 3;

    /* system functions */

    public function getClassName() {
        return "Sessies";
    }

    public function getUid() {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id) {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

    public function getDropdownDefaultValue() {
        
    }

    public function getDropdownNullOption() {
        
    }

    public function getIntValueMethod() {
        
    }

    public function getValue() {
        $racedagKlasse = $this->getRacedagKlasse();
        $s = $racedagKlasse->getValue();
        return $s . ' ' . $this->getSessieNaam();
    }

    function __toString() {
        return $this->getValue();
    }

}
