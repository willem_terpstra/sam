<?php

class Klasse extends _Klasse implements DropdownAble {

    public function getDropdownDefaultValue() {
        return '--selecteer--';
    }

    public function getDropdownNullOption() {
        return NULL;
    }

    public function getIntValueMethod() {
        return 'getUid';
    }

    public function getValue() {
        return $this->naam;
    }
    function __toString() {
        return $this->getValue();
    }
    
    /* system functions */

    public function getClassName() {
        return "Klasse";
    }

    public function getUid() {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id) {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}

?>