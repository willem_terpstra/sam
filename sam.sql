/*
SQLyog Enterprise - MySQL GUI v8.22 
MySQL - 5.5.28 : Database - sam
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `auth` */

CREATE TABLE `auth` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `naam` char(32) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `auth` */

insert  into `auth`(`uid`,`level`,`naam`) values (1,1,'gast'),(2,2,'user'),(4,4,'admin'),(5,5,'super'),(6,3,'demo');

/*Table structure for table `class_auth` */

CREATE TABLE `class_auth` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_names_id` int(10) unsigned NOT NULL,
  `browse_auth_id` int(10) unsigned NOT NULL,
  `browse_group_id` int(10) unsigned NOT NULL DEFAULT '1',
  `add_auth_id` int(10) unsigned NOT NULL,
  `add_group_id` int(10) unsigned NOT NULL,
  `update_auth_id` int(10) unsigned NOT NULL,
  `update_group_id` int(10) unsigned NOT NULL DEFAULT '1',
  `detail_auth_id` int(10) unsigned NOT NULL,
  `detail_group_id` int(10) unsigned NOT NULL,
  `browselijst_auth_id` int(10) unsigned NOT NULL,
  `browselijst_group_id` int(10) unsigned NOT NULL,
  `editlijst_auth_id` int(10) unsigned NOT NULL,
  `editlijst_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `NewIndex1` (`class_names_id`),
  KEY `FK_class_auth_browse` (`browse_auth_id`),
  KEY `FK_class_auth_update` (`update_auth_id`),
  KEY `FK_class_auth_browse_group` (`browse_group_id`),
  KEY `FK_class_auth_update_group` (`update_group_id`),
  KEY `FK_class_auth_browselijst` (`browselijst_auth_id`),
  KEY `FK_class_auth_browselijst_group` (`browselijst_group_id`),
  KEY `FK_class_auth_detail_auth` (`detail_auth_id`),
  KEY `FK_class_auth_detail_group` (`detail_group_id`),
  KEY `FK_class_auth_editlijst_auth` (`editlijst_auth_id`),
  KEY `FK_class_auth_editlijst_group` (`editlijst_group_id`),
  KEY `FK_class_auth_add_auth` (`add_auth_id`),
  KEY `FK_class_auth_add_group` (`add_group_id`),
  CONSTRAINT `FK_class_auth_add_auth` FOREIGN KEY (`add_auth_id`) REFERENCES `auth` (`uid`),
  CONSTRAINT `FK_class_auth_add_group` FOREIGN KEY (`add_group_id`) REFERENCES `groups` (`uid`),
  CONSTRAINT `FK_class_auth_browse` FOREIGN KEY (`browse_auth_id`) REFERENCES `auth` (`uid`),
  CONSTRAINT `FK_class_auth_browselijst` FOREIGN KEY (`browselijst_auth_id`) REFERENCES `auth` (`uid`),
  CONSTRAINT `FK_class_auth_browselijst_group` FOREIGN KEY (`browselijst_group_id`) REFERENCES `groups` (`uid`),
  CONSTRAINT `FK_class_auth_browse_group` FOREIGN KEY (`browse_group_id`) REFERENCES `groups` (`uid`),
  CONSTRAINT `FK_class_auth_class_names` FOREIGN KEY (`class_names_id`) REFERENCES `class_names` (`uid`),
  CONSTRAINT `FK_class_auth_detail_auth` FOREIGN KEY (`detail_auth_id`) REFERENCES `auth` (`uid`),
  CONSTRAINT `FK_class_auth_detail_group` FOREIGN KEY (`detail_group_id`) REFERENCES `groups` (`uid`),
  CONSTRAINT `FK_class_auth_editlijst_auth` FOREIGN KEY (`editlijst_auth_id`) REFERENCES `auth` (`uid`),
  CONSTRAINT `FK_class_auth_editlijst_group` FOREIGN KEY (`editlijst_group_id`) REFERENCES `groups` (`uid`),
  CONSTRAINT `FK_class_auth_update` FOREIGN KEY (`update_auth_id`) REFERENCES `auth` (`uid`),
  CONSTRAINT `FK_class_auth_update_group` FOREIGN KEY (`update_group_id`) REFERENCES `groups` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

/*Data for the table `class_auth` */

insert  into `class_auth`(`uid`,`class_names_id`,`browse_auth_id`,`browse_group_id`,`add_auth_id`,`add_group_id`,`update_auth_id`,`update_group_id`,`detail_auth_id`,`detail_group_id`,`browselijst_auth_id`,`browselijst_group_id`,`editlijst_auth_id`,`editlijst_group_id`) values (1,6,4,1,4,1,4,1,4,1,4,1,4,1),(2,7,1,1,4,1,1,1,1,1,4,1,4,1),(3,20,1,1,4,1,4,1,4,1,1,1,4,1),(4,3,4,1,4,1,4,1,4,1,4,1,4,1),(5,2,4,1,4,1,4,1,4,1,4,1,4,1),(6,24,1,1,2,1,2,2,2,2,1,1,2,2),(7,23,1,1,2,2,2,2,2,2,1,1,2,2),(8,26,1,1,2,2,2,2,2,2,1,1,2,2),(9,9,4,1,4,1,4,1,4,1,4,1,4,1),(10,5,2,2,2,1,2,2,2,2,4,1,4,2),(11,25,1,1,1,1,1,1,1,1,4,1,4,1),(12,1,1,1,4,1,4,1,4,1,4,1,4,1),(13,14,2,1,2,1,2,2,2,2,2,1,2,2),(14,15,2,2,2,2,2,2,2,2,2,2,2,2),(15,16,1,1,4,1,4,1,4,1,4,1,4,1),(16,17,1,1,4,1,4,1,4,1,4,1,4,1),(17,18,1,1,4,1,4,1,4,1,4,1,4,1),(18,19,1,1,4,1,4,1,4,1,4,1,4,1),(19,21,1,1,4,1,4,1,4,1,4,1,4,1),(20,27,1,1,4,1,4,1,4,1,4,1,4,1),(21,28,1,1,4,1,4,1,4,1,4,1,4,1),(22,29,1,1,4,1,4,1,4,1,4,1,4,1),(23,30,1,1,4,1,4,1,4,1,4,1,4,1),(24,31,1,1,2,2,2,2,2,2,1,1,2,2),(25,32,1,1,2,2,2,2,2,2,1,1,2,2),(26,33,1,1,4,1,1,1,4,1,1,1,4,1),(27,35,1,1,4,1,4,1,4,1,1,1,4,1),(28,36,1,1,4,1,4,1,4,1,4,1,4,1),(29,38,4,1,4,1,4,1,4,1,4,1,4,1),(30,39,1,1,2,2,2,2,2,2,1,1,2,2),(31,41,1,1,2,1,2,2,2,2,1,1,4,1),(32,42,4,1,4,1,4,1,4,1,4,1,4,1),(33,43,1,1,2,2,2,2,2,2,1,1,2,2),(34,44,1,1,4,1,4,1,4,1,4,1,4,1),(35,45,1,1,2,1,2,2,2,2,1,1,2,2),(36,46,1,1,2,1,2,2,2,2,1,1,2,2),(37,47,4,2,4,2,4,2,4,2,4,2,4,2),(38,48,4,2,4,2,4,2,4,2,4,2,4,2),(39,49,2,1,2,2,2,2,4,1,2,1,4,2);

/*Table structure for table `import` */

CREATE TABLE `import` (
  `uid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) DEFAULT NULL,
  `tijd` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `import` */

insert  into `import`(`uid`,`naam`,`tijd`) values (1,NULL,NULL);

/*Table structure for table `import_file` */

CREATE TABLE `import_file` (
  `uid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `sessies_id` bigint(10) unsigned DEFAULT NULL,
  `status` enum('uploaded','verwerkt') DEFAULT 'uploaded',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `FK_import_file_sessies` (`sessies_id`),
  CONSTRAINT `FK_import_file_sessies` FOREIGN KEY (`sessies_id`) REFERENCES `sessies` (`uid`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8;

/*Data for the table `import_file` */

insert  into `import_file`(`uid`,`sessies_id`,`status`,`name`) values (15,NULL,'verwerkt','080113-250cc_-_1e_Manche (1).csv'),(16,NULL,'uploaded','080113-250cc_-_1e_Manche (1).csv'),(18,NULL,'verwerkt','280213-250cc   350cc - 1e Manche.csv'),(44,NULL,'uploaded','150313-'),(51,NULL,'uploaded','150313-'),(52,NULL,'uploaded','150313-'),(63,NULL,'uploaded','150313-'),(71,NULL,'uploaded','150313-'),(72,NULL,'uploaded','150313-'),(73,NULL,'uploaded','160313-'),(74,NULL,'uploaded','160313-'),(76,NULL,'uploaded','160313-'),(80,NULL,'uploaded','160313-'),(85,NULL,'uploaded','170313-'),(91,NULL,'uploaded','190313-'),(92,NULL,'uploaded','190313-'),(100,NULL,'uploaded','200313-'),(111,94,'verwerkt','200313-500cc   750cc - 1e Manche.csv'),(115,96,'verwerkt','200313-500cc   750cc - 2e Manche.csv'),(119,97,'verwerkt','200313-50cc   125cc - training.csv'),(120,100,'verwerkt','200313-1000cc - training.csv'),(122,102,'verwerkt','200313-1000cc - 2e Manche.csv'),(123,106,'verwerkt','200313-50cc   125cc - training.csv'),(125,107,'verwerkt','200313-50cc   125cc - 1e Manche.csv'),(126,101,'verwerkt','210313-1000cc - 1e Manche.csv'),(133,123,'verwerkt','230313-250cc - 1e manche.csv'),(136,132,'verwerkt','270313-50cc   125cc - training.csv'),(138,135,'verwerkt','270313-50cc   125cc - 2e Manche.csv');

/*Table structure for table `klasse` */

CREATE TABLE `klasse` (
  `uid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `klasse` */

insert  into `klasse`(`uid`,`naam`) values (1,'50 cc'),(2,'125 cc'),(3,'250 cc'),(4,'350 cc'),(5,'500 cc'),(6,'750 cc'),(7,'YTR'),(8,'Endurance'),(9,'Zijspannen'),(10,'Clubklasse 1'),(11,'Clubclasse 2'),(12,'CMDT'),(13,'CMT');

/*Table structure for table `meta_fields_settings` */

CREATE TABLE `meta_fields_settings` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `setting` enum('obliged','hidden','label_nl','type') DEFAULT NULL,
  `value` text,
  `validation` enum('postcode','telefoon','mobiel','datum_nl','email') DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `idx_meta_field_settings` (`setting`,`field_name`,`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `meta_fields_settings` */

/*Table structure for table `meta_table_settings` */

CREATE TABLE `meta_table_settings` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_table` char(255) NOT NULL,
  `create_entity` tinyint(1) DEFAULT '1',
  `create_controller` tinyint(1) DEFAULT '1',
  `create_html` tinyint(1) DEFAULT '1',
  `header_text` char(255) DEFAULT NULL,
  `create_view` tinyint(1) DEFAULT '1',
  `create_frontend` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `idx_unq_meta_table_settings` (`target_table`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

/*Data for the table `meta_table_settings` */

insert  into `meta_table_settings`(`uid`,`target_table`,`create_entity`,`create_controller`,`create_html`,`header_text`,`create_view`,`create_frontend`) values (10,'meta_field_settings',0,0,0,NULL,0,0),(11,'meta_table_settings',0,0,0,NULL,0,0),(25,'klasse',1,1,1,NULL,1,1),(24,'resultaat',1,1,1,NULL,1,1),(26,'import',1,1,1,NULL,1,1),(27,'auth',1,1,1,NULL,1,1),(28,'import_file',1,1,1,NULL,1,1),(29,'sessie_naam',1,1,1,NULL,1,1),(30,'racedag',1,1,1,NULL,1,1),(31,'racedag_klasse',1,1,1,NULL,1,1),(32,'sessies',1,1,1,NULL,1,1),(33,'vw_klasse_totalen',1,1,1,NULL,1,1),(34,'vw_rondes_resultaat',1,1,1,NULL,1,1),(35,'vw_uitslag',1,1,1,NULL,1,1);

/*Table structure for table `racedag` */

CREATE TABLE `racedag` (
  `uid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `datum` date DEFAULT NULL,
  `naam` varchar(255) DEFAULT NULL,
  `min_ronden_perc` int(11) DEFAULT '70',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `racedag` */

insert  into `racedag`(`uid`,`datum`,`naam`,`min_ronden_perc`) values (14,'2013-03-20','Harry',60),(15,'2013-05-04','Harderwijk/Hierden',70),(17,'2013-05-11','Raalte/Luttenberg',70);

/*Table structure for table `racedag_klasse` */

CREATE TABLE `racedag_klasse` (
  `uid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `racedag_id` bigint(10) unsigned DEFAULT NULL,
  `klasse_id` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `unq_racedag_klasse` (`racedag_id`,`klasse_id`),
  KEY `FK_racedag_klasse_klasse` (`klasse_id`),
  CONSTRAINT `FK_racedag_klasse_klasse` FOREIGN KEY (`klasse_id`) REFERENCES `klasse` (`uid`),
  CONSTRAINT `FK_racedag_klasse_racedag` FOREIGN KEY (`racedag_id`) REFERENCES `racedag` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8;

/*Data for the table `racedag_klasse` */

insert  into `racedag_klasse`(`uid`,`racedag_id`,`klasse_id`) values (2,NULL,NULL),(3,NULL,NULL),(78,14,1),(217,14,2),(218,14,3),(219,14,4),(220,14,5),(77,14,6),(222,14,7),(224,14,9),(79,14,10),(80,14,11),(227,14,12),(228,14,13),(105,15,1),(106,15,2),(104,15,3),(108,15,4),(109,15,5),(110,15,6),(141,15,7),(142,15,8),(113,15,9),(116,15,10),(145,15,11),(118,17,1),(117,17,2),(119,17,3),(123,17,4),(124,17,5),(125,17,6),(127,17,8),(128,17,9),(129,17,10),(130,17,11);

/*Table structure for table `resultaat` */

CREATE TABLE `resultaat` (
  `uid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `plaats` int(5) unsigned DEFAULT NULL,
  `naam` varchar(255) DEFAULT NULL,
  `msec_straf` int(10) DEFAULT NULL,
  `msec_gemiddeld` int(10) DEFAULT NULL,
  `rondes` int(5) DEFAULT NULL,
  `import_file_id` bigint(10) unsigned NOT NULL,
  `qualified` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `FK_resultaat_import_file` (`import_file_id`),
  CONSTRAINT `FK_resultaat_import_file` FOREIGN KEY (`import_file_id`) REFERENCES `import_file` (`uid`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=532 DEFAULT CHARSET=utf8;

/*Data for the table `resultaat` */

insert  into `resultaat`(`uid`,`plaats`,`naam`,`msec_straf`,`msec_gemiddeld`,`rondes`,`import_file_id`,`qualified`) values (25,NULL,'Barry Derks',7720,75979,13,15,0),(26,NULL,'Tjerk de Boer',5700,78939,13,15,0),(27,NULL,'Joop  van de Pol',17786,75125,13,15,0),(28,NULL,'Martin Eijbersen',13412,80539,13,15,0),(29,NULL,'Joan van de Pol',9633,83718,12,15,0),(30,NULL,'Gerrit Butter',5158,86652,9,15,0),(31,NULL,'Gerrit Butter',5891,130042,7,18,0),(32,NULL,'Martin Eijbersen',5067,135752,7,18,0),(33,NULL,'Albert den Hartog',5072,135093,7,18,0),(34,NULL,'Gerrit Bekker',10229,138086,7,18,0),(35,NULL,'Mark de Vink',11212,141088,7,18,0),(36,NULL,'Jan vd Zanden',11627,157986,6,18,0),(375,NULL,'Bert Struijk',13460,120352,8,111,1),(376,NULL,'Joost Ris',18857,120171,8,111,1),(377,NULL,'Dirk Ris',11723,124323,8,111,1),(378,NULL,'Jan Wagemakers',7477,129225,8,111,1),(379,NULL,'Erwin Moes',28496,132507,8,111,1),(380,NULL,'Bort Koelewijn',28219,126940,8,111,1),(381,NULL,'Joop van der Have',29203,131395,8,111,1),(382,NULL,'Harry Althof',13155,137965,7,111,1),(383,NULL,'Ted Haanappel',11403,136166,7,111,1),(384,NULL,'Mark de Vink',8495,135218,7,111,1),(415,NULL,'Joost Ris',5066,115709,8,115,1),(416,NULL,'Dirk Ris',9210,117811,8,115,1),(417,NULL,'Bert Struijk',14496,119059,8,115,1),(418,NULL,'Bort Koelewijn',2819,123901,8,115,1),(419,NULL,'Jan Wagemakers',2868,126570,5,115,1),(420,NULL,'Ted Haanappel',8895,127207,8,115,1),(421,NULL,'Erwin Moes',11380,125649,8,115,1),(422,NULL,'Joop van der Have',4450,128293,8,115,1),(423,NULL,'Mark de Vink',20073,125240,8,115,1),(424,NULL,'Harry Althof',13966,132399,7,115,1),(430,NULL,'Willem Heykoop',7078,131438,7,123,1),(431,NULL,'Wil Doodeman',5757,143232,7,123,1),(432,NULL,'Mart van Wetten',6381,148668,7,123,1),(433,NULL,'Hans de Wit',6100,148713,7,123,1),(434,NULL,'Guido de Wit',7204,150741,7,123,1),(435,NULL,'42 Willem Heykoop',7078,131438,7,126,1),(436,NULL,'54 Wil Doodeman',5757,143232,7,126,1),(437,NULL,'25 Mart van Wetten',6381,148668,7,126,1),(438,NULL,'36 Hans de Wit',6100,148713,7,126,1),(439,NULL,'96 Guido de Wit',7204,150741,7,126,1),(452,NULL,'253 Barry Derks',7720,75979,13,133,1),(453,NULL,'219 Tjerk de Boer',5700,78939,13,133,1),(454,NULL,'272 Joop  van de Pol',17786,75125,13,133,1),(455,NULL,'277 Martin Eijbersen',13412,80539,13,133,1),(456,NULL,'268 Joan van de Pol',9633,83718,12,133,1),(457,NULL,'222 Gerrit Butter',5158,86652,9,133,1),(469,NULL,'Willem Heykoop',7078,131438,7,136,1),(470,NULL,'Wil Doodeman',5757,143232,7,136,1),(471,NULL,'Mart van Wetten',6381,148668,7,136,1),(472,NULL,'Hans de Wit',6100,148713,7,136,1),(473,NULL,'Guido de Wit',7204,150741,7,136,1),(480,NULL,'Willem Heykoop',4639,130736,6,138,1),(481,NULL,'Wil Doodeman',5378,140009,6,138,1),(482,NULL,'Mart van Wetten',9703,147720,6,138,1),(483,NULL,'Hans de Wit',4889,149615,6,138,1),(484,NULL,'Guido de Wit',2901,149250,6,138,1),(527,NULL,'Willem Heykoop',7078,131438,7,119,1),(528,NULL,'Wil Doodeman',5757,143232,7,119,1),(529,NULL,'Mart van Wetten',6381,148668,7,119,1),(530,NULL,'Hans de Wit',6100,148713,7,119,1),(531,NULL,'Guido de Wit',7204,150741,7,119,1);

/*Table structure for table `sessie_naam` */

CREATE TABLE `sessie_naam` (
  `uid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `sessie_naam` */

insert  into `sessie_naam`(`uid`,`naam`) values (1,'training'),(2,'1e manche'),(3,'2e manche');

/*Table structure for table `sessies` */

CREATE TABLE `sessies` (
  `uid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `racedag_klasse_id` bigint(10) unsigned DEFAULT NULL,
  `sessie_naam_id` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `unq_racedag_klasse_sessie_naam` (`racedag_klasse_id`,`sessie_naam_id`),
  KEY `FK_sessies_sessie_naam` (`sessie_naam_id`),
  CONSTRAINT `FK_sessies_racedag_klasse` FOREIGN KEY (`racedag_klasse_id`) REFERENCES `racedag_klasse` (`uid`),
  CONSTRAINT `FK_sessies_sessie_naam` FOREIGN KEY (`sessie_naam_id`) REFERENCES `sessie_naam` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;

/*Data for the table `sessies` */

insert  into `sessies`(`uid`,`racedag_klasse_id`,`sessie_naam_id`) values (15,NULL,1),(16,NULL,1),(17,NULL,1),(94,77,1),(154,77,2),(96,77,3),(97,78,1),(98,78,2),(99,78,3),(100,79,1),(101,79,2),(102,79,3),(106,80,1),(107,80,2),(108,80,3),(140,104,1),(123,104,2),(139,104,3),(125,105,1),(126,105,2),(127,105,3),(136,106,1),(137,106,2),(138,106,3),(128,109,1),(129,109,2),(130,109,3),(132,118,1),(134,118,2),(135,118,3);

/*Table structure for table `vw_klasse_totalen` */

DROP TABLE IF EXISTS `vw_klasse_totalen`;

/*!50001 CREATE TABLE  `vw_klasse_totalen`(
 `uid` bigint(10) unsigned ,
 `racedag_id` bigint(10) unsigned ,
 `klasse_id` bigint(10) unsigned ,
 `racedag_klasse_id` bigint(10) unsigned ,
 `sessies_id` bigint(10) unsigned ,
 `racedag_naam` varchar(255) ,
 `racedag_datum` date ,
 `klasse_naam` varchar(255) ,
 `sessie_naam` varchar(255) ,
 `resultaat_rondes` int(5) ,
 `msec_gemiddeld` int(10) ,
 `msec_straf` int(10) ,
 `resultaat_naam` varchar(255) ,
 `sessie_naam_id` bigint(10) unsigned 
)*/;

/*Table structure for table `vw_rondes_resultaat` */

DROP TABLE IF EXISTS `vw_rondes_resultaat`;

/*!50001 CREATE TABLE  `vw_rondes_resultaat`(
 `uid` bigint(10) unsigned ,
 `resultaat_naam` varchar(255) ,
 `sessies_id` bigint(10) unsigned ,
 `sessie_naam` varchar(255) ,
 `rondes` int(5) ,
 `max_rondes` bigint(11) ,
 `rondes_perc` decimal(17,4) 
)*/;

/*Table structure for table `vw_uitslag` */

DROP TABLE IF EXISTS `vw_uitslag`;

/*!50001 CREATE TABLE  `vw_uitslag`(
 `uid` bigint(10) unsigned ,
 `racedag_klasse_id` bigint(10) unsigned ,
 `resultaat_naam` varchar(255) ,
 `straf` decimal(32,0) ,
 `klasse_naam` varchar(255) ,
 `rondes` decimal(32,0) ,
 `racedag_naam` varchar(255) ,
 `racedag_datum` date 
)*/;

/*View structure for view vw_klasse_totalen */

/*!50001 DROP TABLE IF EXISTS `vw_klasse_totalen` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_klasse_totalen` AS (select `resultaat`.`uid` AS `uid`,`racedag`.`uid` AS `racedag_id`,`racedag_klasse`.`klasse_id` AS `klasse_id`,`racedag_klasse`.`uid` AS `racedag_klasse_id`,`sessies`.`uid` AS `sessies_id`,`racedag`.`naam` AS `racedag_naam`,`racedag`.`datum` AS `racedag_datum`,`klasse`.`naam` AS `klasse_naam`,`sessie_naam`.`naam` AS `sessie_naam`,`resultaat`.`rondes` AS `resultaat_rondes`,`resultaat`.`msec_gemiddeld` AS `msec_gemiddeld`,`resultaat`.`msec_straf` AS `msec_straf`,`resultaat`.`naam` AS `resultaat_naam`,`sessie_naam`.`uid` AS `sessie_naam_id` from ((((((`sessies` join `sessie_naam` on((`sessies`.`sessie_naam_id` = `sessie_naam`.`uid`))) join `import_file` on((`import_file`.`sessies_id` = `sessies`.`uid`))) join `resultaat` on((`resultaat`.`import_file_id` = `import_file`.`uid`))) join `racedag_klasse` on((`sessies`.`racedag_klasse_id` = `racedag_klasse`.`uid`))) join `klasse` on((`racedag_klasse`.`klasse_id` = `klasse`.`uid`))) join `racedag` on((`racedag_klasse`.`racedag_id` = `racedag`.`uid`))) where (`sessie_naam`.`uid` <> 1) order by `resultaat`.`naam`) */;

/*View structure for view vw_rondes_resultaat */

/*!50001 DROP TABLE IF EXISTS `vw_rondes_resultaat` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rondes_resultaat` AS select `resultaat`.`uid` AS `uid`,`resultaat`.`naam` AS `resultaat_naam`,`import_file`.`sessies_id` AS `sessies_id`,`sessie_naam`.`naam` AS `sessie_naam`,`resultaat`.`rondes` AS `rondes`,(select max(`resultaat`.`rondes`) from `resultaat` where (`resultaat`.`import_file_id` = `import_file`.`uid`)) AS `max_rondes`,(100 * (`resultaat`.`rondes` / (select max(`resultaat`.`rondes`) from `resultaat` where (`resultaat`.`import_file_id` = `import_file`.`uid`)))) AS `rondes_perc` from (((`resultaat` join `import_file` on((`resultaat`.`import_file_id` = `import_file`.`uid`))) join `sessies` on((`import_file`.`sessies_id` = `sessies`.`uid`))) join `sessie_naam` on((`sessies`.`sessie_naam_id` = `sessie_naam`.`uid`))) */;

/*View structure for view vw_uitslag */

/*!50001 DROP TABLE IF EXISTS `vw_uitslag` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_uitslag` AS select `vw_klasse_totalen`.`uid` AS `uid`,`vw_klasse_totalen`.`racedag_klasse_id` AS `racedag_klasse_id`,`vw_klasse_totalen`.`resultaat_naam` AS `resultaat_naam`,sum(`vw_klasse_totalen`.`msec_straf`) AS `straf`,`vw_klasse_totalen`.`klasse_naam` AS `klasse_naam`,sum(`vw_klasse_totalen`.`resultaat_rondes`) AS `rondes`,`vw_klasse_totalen`.`racedag_naam` AS `racedag_naam`,`vw_klasse_totalen`.`racedag_datum` AS `racedag_datum` from ((`vw_klasse_totalen` join `sessies` on((`vw_klasse_totalen`.`sessies_id` = `sessies`.`uid`))) join `sessie_naam` on((`sessies`.`sessie_naam_id` = `sessie_naam`.`uid`))) where (`sessie_naam`.`naam` in ('1e manche','2e manche')) group by `vw_klasse_totalen`.`resultaat_naam`,`vw_klasse_totalen`.`racedag_naam` order by `vw_klasse_totalen`.`racedag_klasse_id`,sum(`vw_klasse_totalen`.`msec_straf`) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
