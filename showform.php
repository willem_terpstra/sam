<?php

require_once 'autoload.php';

if (!$config = simplexml_load_file('MVC/sys/config/config.xml')) {
    trigger_error('Error reading XML file', E_USER_ERROR);
}
date_default_timezone_set('Europe/Paris');
mysql_connect("localhost", $config->mysql->user, $config->mysql->pwd);
mysql_select_db($config->mysql->db);
$uid = NULL;
if(array_key_exists('uid', $_POST)) {
    if(!empty($_POST['uid']))
        $uid = $_POST['uid'];
}
if(array_key_exists('id', $_GET)) {
    if(!empty($_GET['id']))
        $uid = $_GET['id'];
}
$viewName=$_GET['e'] . 'View';
$view = new $viewName($uid);
echo $view->detail();
?> 
