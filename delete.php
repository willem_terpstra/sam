<?php

require_once 'autoload.php';

if (!$config = simplexml_load_file('MVC/sys/config/config.xml')) {
    trigger_error('Error reading XML file', E_USER_ERROR);
}
date_default_timezone_set('Europe/Paris');
mysql_connect("localhost", $config->mysql->user, $config->mysql->pwd);
mysql_select_db($config->mysql->db);
$uid = NULL;
if (array_key_exists('uid', $_POST)) {
    if (!empty($_POST['uid']))
        $uid = $_POST['uid'];
}
$html = '';
$viewName = $_GET['e'] . 'View';
$contentType = 'text/html; charset=utf-8';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
}
header("Content-type: $contentType");
try {
    $view = new $viewName($uid);
    $view->remove();
} catch (dbTableException $e) {
    if ($e->getCode() == dbTableException::LOAD_FAILED_EXCEPTION) {
        $html = 'error loading ' . $_GET['e'];
        echo json_encode(
                array(
                    'detail' => $html,
                )
        );
        exit;
    }
}

$msgs = $view->getMessages();
if (!empty($msgs)) {
    $html = $view->detail();
//    echo $html;
    echo json_encode(
            array(
                'detail' => $html,
            )
    );
    exit;
}
$view = new $viewName();
$html .= $view->browseLijst();

echo json_encode(
        array(
            'detail' => '',
            'lijst' => $html
        )
);

