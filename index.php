<?php

session_start();

require_once 'autoload.php';
set_error_handler("errorHandler");
if (!$config = simplexml_load_file('MVC/sys/config/config.xml')) {
    trigger_error('Error reading XML file', E_USER_ERROR);
}
//	error_reporting($config->error_reporting);

date_default_timezone_set('Europe/Paris');

$subDir = $config->install_dir;
mysql_connect("localhost", $config->mysql->user, $config->mysql->pwd);
mysql_select_db($config->mysql->db);

$post = new Post();
$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$urlArr = parse_url($url);

$path = $urlArr['path'];
$request = explode('/', $path);
if (empty($request[1]) && empty($request[2])) {
    $t = new TemplateEngine('page.html');
    $t->assign('content', 'TEST');
    echo $t->execTemplate();
//		header('Location: /home.php');
    exit;
}
$_command = strtolower($request[1]);
$_target = $request[2];

if (empty($_command)) {
    $_command = 'browselijst';
}
if (empty($_target)) {
    $_target = 'Racedag';
}
$_target = ucfirst($_target);

if (strstr($url, '.png') || strstr($url, '.gif') || strstr($url, '.jpg')) { // send image
    header("Content-Type: image/jpeg");
    foreach ($config->image_paths->path as $path) {
        if (file_exists($path . $request[count($request) - 1])) {
            echo file_get_contents($path . $request[count($request) - 1]);
            exit;
        }
    }
    exit;
}

if (strstr($url, '.js') || strstr($url, '.css')) { // send js
    header('Content-type: application/javascript');
    foreach ($config->js_paths->path as $path) {
        if (file_exists($path . $request[count($request) - 1])) {
            echo file_get_contents($path . $request[count($request) - 1]);
            exit;
        }
    }
    exit;
}

$uid = NULL;
if (isset($request[3])) {
    $_uid = $request[3];
    if (is_numeric($_uid)) {
        $uid = (int) $_uid;
    }
} else {
    if(isset($post['uid'])) {
        $uid = $post['uid'];
    }
}
$controller = NULL;
$viewName = NULL;

switch ($_target) {
    default:
        if (!empty($_target)) {
            $controllerName = $_target . 'Controller';
            $controller = new $controllerName();
            $viewName = $_target . 'View';
        }
}
$action = NULL;
switch ($_command) {
    case 'add':
    case 'edit':
        $action = Controller::DETAIL;
        break;
    case 'cancel':
        $action = Controller::CANCEL;
        break;
    case 'browse':
        $action = Controller::BROWSE;
        break;
    case 'browselijst':
        $action = Controller::BROWSELIJST;
        break;
    case 'update':
        $action = Controller::UPDATE;
        break;
    case 'editlijst':
        $action = Controller::EDITLIJST;
        break;
    case 'remove':
        $action = Controller::REMOVE;
        break;
    case 'delete':
        $action = Controller::DELETE;
        break;

    case 'hierarchy':
        $action = Controller::SELF_REFERRING_LIST;
        break;
    case 'load':
        $action = Controller::LOAD;
        break;
    case 'search':
        $action = Controller::SEARCH;
        break;
    case 'sessies':
        $action = RacedagKlasseController::SESSIES;
        break;
    case 'add_all_klasses':
        $action = RacedagKlasseController::ADD_ALL_KLASSES;
        break;
    case 'add_all_sessies':
        $action = SessiesController::ADD_ALL_SESSIES;
        break;
}

$mainHeader = '';
if (!is_null($controller)) {
    $content = $controller->run($viewName, $uid, $action);
//    $script = $controller->javascript();
    $t = new TemplateEngine('page.html');
    $t->assign('content', $content);
//    $t->assign('script', $script);
    echo $t->execTemplate();
    exit;
} else {

    $content = 'Controller not found';
}
$t = new TemplateEngine('empty.php');
$t->assign('mainHeader', $mainHeader);
//if (Authorization::getAuthLevel() >= Auth::ADMIN) {
//    $t->assign('beheer', 1);
//}

$t->assign('content', $content);
echo $t->execTemplate();

function errorHandler($errno, $errstr, $errfile, $errline) {
    echo( "errno: $errno<br/> err: $errstr<br/> file: $errfile line $errline<br>");
}

?>