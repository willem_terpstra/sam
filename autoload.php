<?php
	if(isset($_GET['XDEBUG_SESSION_START'])) {
		$path = dirname(realpath(__FILE__));
	} else {
		$path = $_SERVER['DOCUMENT_ROOT'];
	}

	$includePaths = array (
		"$path/MVC/sys/classes",
		"$path/MVC/sys/classes/lib",
		"$path/MVC/sys/classes/exceptions",
		"$path/MVC/sys/entity",
		"$path/MVC/sys/entity/generated",
		"$path/MVC/classes",
		"$path/MVC/classes/Exceptions",
		"$path/MVC/sys/interfaces",
		"$path/MVC/entity",
		"$path/MVC/entity/generated",
		"$path/MVC/views",
		"$path/MVC/views/custom",
		"$path/MVC/views/generated",
		"$path/MVC/html",
		"$path/MVC/controllers",
		"$path/MVC/controllers/custom",
		"$path/MVC",
	);
	
	foreach ( $includePaths as $includePath )
	{	$includePath = str_replace('/','\\',$includePath);
		set_include_path($includePath . PATH_SEPARATOR . get_include_path() );
	}

	$requireFailed = false;
	function requireHandler($errno, $errstr, $errfile, $errline) {
		global $requireFailed;
		$requireFailed = true;
		echo "$errno, $errstr, $errfile, $errline";
		return true;
	}
	
	function __autoload ( $className )
	{	try {
			set_error_handler("requireHandler");			
			require_once $className . '.php';
			restore_error_handler();			
		}
		catch (Exception $e)
		{	var_export($e);
		}	
	}

?>